#!/bin/bash

echo "POSTGRES_HOST=db" > .env
echo "POSTGRES_PORT=5432" >> .env
echo "POSTGRES_USER=postgres" >> .env
echo "POSTGRES_DB=postgres" >> .env
echo "POSTGRES_PASSWORD=password" >> .env
echo "POSTGRES_DATABASE=postgres" >> .env
echo "REACT_APP_SECRET_KEY=6LeCgCIcAAAAAJ533O0lNHJ5t1ja4Mt9U8OXpbr8" >> .env

if [ "$1" = "--save" ]
then
    echo "DB_PATH=~/volumes/wekinapi/postgresql/:/var/lib/postgresql/data/" >> .env
else
    echo "DB_PATH=/var/lib/postgresql/data/" >> .env
fi
docker-compose down -v && docker-compose build --no-cache && docker-compose up