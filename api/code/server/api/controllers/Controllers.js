const axios = require('axios');
var client_db = null;
var db = require('../../database.js');
const bcrypt = require('bcrypt')
const { v4: uuidv4 } = require('uuid');
var pg = require("pg");
var async = require('async');
var config = require('../../config');
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var crypto = require("crypto")
const fs = require('fs');
var CryptoJS = require("crypto-js");

const GMAIL_ACCOUNT = 'noreply.wekin@gmail.com'
const GMAIL_PASWD = 'noreply35'

var smtpTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: GMAIL_ACCOUNT,
      pass: GMAIL_PASWD
    }
});
var rand,mailOptions,host,link;

client_db = db.connectDb(10, function(status){
    if (status == "error")
        process.exit(84);
    else
        client_db = status;
});

function sendbugEmail(comment){
    smtpTransport.sendMail({
      from: '"Wekin" <'+ GMAIL_ACCOUNT + '>',
      to: "godeffroy.roue@epitech.eu",
      subject: "Rapport de bug",
      text: "Rapport de Bug",
  
      html: "<p>Un nouveau rapport de bug à été reçu venant de l'application mobile</p><p>Le message:</p><p>"+ comment +"</p>",
    }).then(info => {
      console.log("email send to " + info["accepted"])
    }).catch(console.error);
  }

function sendEmailTo(email, username, filepath){
    smtpTransport.sendMail({
      from: '"Wekin" <'+ GMAIL_ACCOUNT + '>',
      to: email,
      subject: "Bienvenue sur Wekin",
      text: "Merci de vous être inscrit sur Wekin",
  
      html: '<h1><span style="color: #2DB4A0;"><strong>Merci '+ username +' d\'avoir rejoint Wekin </strong></span></h1><img src="cid:affiche" alt="wekin" width="800" height="1132" style="display: block;" loading="lazy">',
      attachments: [{
        filename: 'affiche.png',
        path: filepath,
        cid: 'affiche'
      }]
    }).then(info => {
      console.log("email send to " + info["accepted"])
    }).catch(console.error);
  }

function verifyAcess(roleid, ressource, trequest) {
        switch (ressource) {
            case "bilan":
                switch (trequest) {
                    case 'GET': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                    case 'POST': access = true; break;
                    case 'PUT': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                    case 'DELETE': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                    default: access = false;
                }
            case "exercice":
                switch (trequest) {
                    case 'GET': access = true; break;
                    case 'POST': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                    case 'PUT': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                    case 'DELETE': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                }
            case "notification":
                switch (trequest) {
                    case 'GET': access = true; break;
                    case 'POST': access = true; break;
                    case 'PUT': access = true; break;
                    case 'DELETE': access = false; break;
                    default: access = false;
                }
            case "reset_link_code":
                switch (trequest) {
                    case 'GET': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                    case 'POST': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                    case 'PUT': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                    case 'DELETE': (roleid == 0) ? access == true : access = false; break;
                }
            case "calendrier":
                switch (trequest) {
                    case 'GET': access = true; break;
                    case 'POST': access = true; break;
                    case 'PUT': access = true; break;
                    case 'DELETE': (roleid == 0 || roleid == 1) ? access = true : access = false; break;
                }
        }
        return (access);
}


exports.verify_captcha = async(req, res) => {
    axios.post(
        `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.REACT_APP_SECRET_KEY}&response=${req.body.token}`,
        {},
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
          },
        },
    )
    .then(response => {
        console.log(response.data.success)
        if (response.data.success == true) {
            return res.status(200).json("Valid")
        }
        else {
            return res.status(400).json("Unvalid");
        }
    })
    .catch(error => {
        return res.status(400).json("Error during captcha authentification: " + error.message);
    })
}

exports.verifybilanaccess = async(req, res) => {
    var token = req.headers['authorization'];
    console.log(req.headers);
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
        else {
            console.log(decoded.id);
            console.log(decoded.roleid);
            res.status(200).json(verifyAcess(decoded.roleid, 'notification', req.method));
        }
    });
}

/**
 * @api {post} /users/register Request Register User
 * @apiName PostRegister
 * @apiGroup Users
 *
 * @apiParam {String} firstName User firstname.
 * @apiParam {String} lastName User lastName.
 * @apiParam {String} password User password.
 * @apiParam {String} email User email.
 * @apiParam {String} type_account User type_account (PRATICIEN, PATIENT).
 *
 * @apiSuccess (201) {Number} status 201.
 * @apiSuccess (201) {String} message The account has been created.
 * @apiSuccess (201) {String} link_code KINExxxxxx.
 * @apiErrorExample {json} Error-Database-getUserperEmail:
 *     HTTP/1.1 400 User email already exists
 *     {
 *       "status": 400
 *       "message": This email xxx@xxx.xxx address is already registered
 *     }
 * @apiErrorExample {json} Error-Database-Register:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Internal server error
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": email, lastName, firstName, password, type_account are required
 *     }
 * @apiErrorExample {json} Error-Database-Register-Patient-getPractitionerEqualtoLinkCode:
 *     HTTP/1.1 400 Practitioner doesn't exist
 *     {
 *       "status": 400
 *       "message": No practitioner was found using this code={link_code}
 *     }
 */
function praticien_register(req) {
    return new Promise((resolve) => {
        try {
            bcrypt.hash(req.body.password, 5, function(err, pwd) {
                        client_db.query('SELECT id FROM users WHERE email=$1', [req.body.email], function(err, result) {
                            if(result.rows[0])
                                return (resolve({'status': 400, 'message': 'This email [' + req.body.email + '] address is already registered'}));
                            else {
                                bcrypt.hash(req.body.email, 1, function(err, link_code) {
                                    link_code = link_code.substr(link_code.length - 6);
                                    client_db.query('INSERT INTO users (id, roleId, firstName, lastName, email, password, email_confirmed, link_code) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [uuidv4(), 1, req.body.firstName, req.body.lastName, req.body.email, pwd, false, link_code], function(err, result) {
                                        if (result) {
                                            sendEmailTo(req.body.email, req.body.firstName, './assets/images/email_practicient.jpg')
                                            return (resolve({'status': 201, 'message': 'The account has been created', 'link_code': link_code, 'email': req.body.email}));
                                        }
                                        else
                                            return (resolve({'status': 500, 'message': 'Internal server error (' + err + ')'}));
                                    });
                                });
                            }
                        });
            });
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
    
}

exports.test_db = async(req, res) => {
    console.log('postgres://'+process.env.POSTGRES_USER+':'+process.env.POSTGRES_PASSWORD+'@'+process.env.POSTGRES_HOST+'/'+process.env.POSTGRES_DB+'');
    pg.connect('postgres://'+process.env.POSTGRES_USER+':'+process.env.POSTGRES_PASSWORD+'@'+process.env.POSTGRES_HOST+'/'+process.env.POSTGRES_DB+'', function(err, client, done) {
        if (err)
            res.status(400).json("Pas de connection à la db");
        else {
            res.status(200).json("Db connecté");
        }
    });
}

function patient_register(req) {
    return new Promise((resolve) => {
        try {
            bcrypt.hash(req.body.password, 5, function(err, pwd) {
                client_db.query('SELECT id FROM users WHERE email=$1', [req.body.email], function(err, result) {
                    if(result.rows[0])
                        return (resolve({'status': 400, 'message': 'This email [' + req.body.email + '] address is already registered'}));
                    else {
                        client_db.query('SELECT id FROM users WHERE link_code=$1 AND roleId=$2', [req.body.link_code, 1], function(err, result) {
                            if(result.rows[0]) {
                                client_db.query('INSERT INTO users (id, roleId, firstName, lastName, email, password, email_confirmed, link_code) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [uuidv4(), 2, req.body.firstName, req.body.lastName, req.body.email, pwd, false, req.body.link_code], function(err, result) {
                                    if (err) {
                                        return (resolve({'status': 500, 'message': err}));
                                    }
                                    else {
                                        sendEmailTo(req.body.email, req.body.firstName, './assets/images/email_patient.jpg')
                                        return (resolve({'status': 201, 'message': 'The account has been created', 'link_code': req.body.link_code}));
                                    }
                                });
                            }
                            else {
                                return (resolve({'status': 400, 'message': "No practitioner was found using this code="+req.body.link_code}));
                            }
                        });
                    }
                });
            });
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

exports.sendBug = async(req, res) => {
    if (!req.body.comment)
        res.status(400).json({'status': 400, 'message': 'comment is required'});
    sendbugEmail(req.body.comment);
    res.status(200).json({"status": 200, "message": "Bug sent"});
}

function decryptData(passphrase, data) {
    return (CryptoJS.AES.decrypt(
        data,
        passphrase
      ).toString(CryptoJS.enc.Utf8));
}

exports.register = async(req, res) => {
    if (!req.body.email || !req.body.lastName || !req.body.firstName || !req.body.password || !req.body.type_account)
        res.status(400).json({'status': 400, 'message': 'email, lastName, firstName, password, type_account are required'});
    else {
        var type_account = req.body.type_account;
        switch (type_account) {
            case 'PRATICIEN':
                console.log("Creating PRATICIEN ACCOUNT");
                req.body.email = decryptData("email", req.body.email)
                req.body.password = decryptData("password", req.body.password)
                req.body.firstName = decryptData("firstName", req.body.firstName)
                req.body.lastName = decryptData("lastName", req.body.lastName)
                response = await praticien_register(req);
                if (response)
                    res.status(response["status"]).json(response)
                else
                    res.status(400).json("KO");
                break;
            case 'PATIENT':
                console.log("Creating PATIENT ACCOUNT");
                response = await patient_register(req);
                if (response)
                    res.status(response["status"]).json(response)
                else
                    res.status(400).json("KO");
                break;
            default:
                res.status(400).json({'status': 400, 'message': 'this type_account:' + req.body.type_account + 'does not exist'});
        }
    }
}

function verify_login(req) {
    return new Promise((resolve) => {
        try {
            client_db.query('SELECT * FROM users WHERE email=$1', [req.body.email], function(err, result) {
            if (result.rows[0]) {
                query_result = result.rows[0];
                bcrypt.compare(req.body.password, result.rows[0].password, function(err, result) {
                    if (result) {
                        var token = jwt.sign({ id: query_result.id, link_code: query_result.link_code, roleid: query_result.roleid}, config.secret, {
                            expiresIn: 86400 // expires in 24 hours
                          });
                        return (resolve({'status': 200, 'message': 'Successful authentication', 'token': token, auth: true}));
                    }
                    else {
                        return (resolve({'status': 400, 'message': 'This password is not valid for this email: [' + req.body.email + ']'}));
                    }
                });
            }
            else
                return (resolve({'status': 400, 'message': 'No user has been found with this email: [' + req.body.email + ']'}));
            });
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

/**
 * @api {post} /users/login Request Login User
 * @apiName PostLogin
 * @apiGroup Users
 *
 * @apiParam {String} email User email.
 * @apiParam {String} password User password.
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message Successful authentication.
 * @apiSuccess (200) {String} token exemple(eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ).
 * @apiSuccess (200) {Boolean} auth true
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": email, password are required
 *     }
 * @apiErrorExample {json} Error-Database-Login:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Internal server error
 *     }
 * @apiErrorExample {json} Error-Database-ComparePassword:
 *     HTTP/1.1 400 Password not valid
 *     {
 *       "status": 400
 *       "message": This password is not valid for this email: xxx@xxx.com
 *     }
 * @apiErrorExample {json} Error-Database-FindUserWithEmail:
 *     HTTP/1.1 400 User email not found
 *     {
 *       "status": 400
 *       "message": No user has been found with this email: xxx@xxx.com
 *     }
 */
exports.login = async(req, res) => {
    if (!req.body.email || !req.body.password)
        res.status(400).json({'status': 400, 'message': 'email, password are required'});
    else {
        var type_account = req.body.type_account;
        if (type_account == 'PRATICIEN') {
            req.body.email = decryptData("email", req.body.email)
            req.body.password = decryptData("password", req.body.password)
        }
        response = await verify_login(req);
        if (response)
            res.status(response["status"]).json(response);
        else
            res.status(400).json("KO");
    }
}

function change_password(req, id) {
    return new Promise((resolve) => {
        try {
            client_db.query('SELECT * FROM users WHERE id=$1', [id], function(err, result) {
                if (result.rows[0]) {
                    bcrypt.compare(req.body.password, result.rows[0].password, function(err, result) {
                        if (result) {
                            bcrypt.hash(req.body.new_password, 5, function(err, pwd) {
                                client_db.query('UPDATE users SET password=$1 WHERE id=$2', [pwd, id], function(err, result) {
                                    if (result)
                                        return (resolve({'status': 200, 'message': 'You password has been modified'}));
                                    else
                                        return (resolve({'status': 200, 'message': 'Error during password modificating'}));
                                });
                            });
                        }
                        else {
                            return (resolve({'status': 400, 'message': 'This password is not valid for this account'}));
                        }
                    });
                }
                else {
                    return (resolve({'status': 400, 'message': 'No user has been found'}));
                }
            });
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

function encryptData(passphrase, data){
    return (CryptoJS.AES.encrypt(
        data,
        passphrase,
      ).toString());
}

/**
 * @api {get} /users/ Request Get User
 * @apiName GetUser
 * @apiGroup Users
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 *
 * @apiSuccess (200) {UUID} id Uuid user account.
 * @apiSuccess (200) {Number} roleid User roleid (PRATICIEN:1, PATIENT:2).
 * @apiSuccess (200) {String} firstName User firstName.
 * @apiSuccess (200) {String} lastName User lastName.
 * @apiSuccess (200) {String} email User email.
 * @apiSuccess (200) {Password} password Hashed user password.
 * @apiSuccess (200) {Boolean} email_confirmed (True, False).
 * @apiSuccess (200) {String} link_code User link_code.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Database-GetUser:
 *     HTTP/1.1 400 User doesn't exist
 *     {
 *       "status": 400
 *       "message": User not found
 *     }
 */
exports.manage_users_get = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
        else {
            client_db.query('SELECT * FROM users WHERE id=$1', [decoded.id], function(err, result) {
                if (result.rows[0])
                    res.status(200).json(result.rows[0]);
                else
                    res.status(400).json({status: 500, message: "User not found"});
            });
        }
    });
}

exports.getFile = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
        else {
            if (!req.query.exerciceid)
                return res.status(400).json({'status': 400, 'message': 'exerciceid is required'});
            else {
                client_db.query('SELECT * FROM exercice WHERE id=$1', [req.query.exerciceid], function(err, result) {
                    if (result.rows[0]) {
                        return res.sendFile('/server/api/ressources/' + result.rows[0].urlassets);
                    }
                    else
                        return res.status(400).json({status: 500, message: "File not found"});
                });
            }
        }
    });
}

/**
 * @api {delete} /users/ Request Delete User
 * @apiName DeleteUser
 * @apiGroup Users
 *
 * @apiSuccess (200) {UUID} id Uuid user account.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": uuid key is provided
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error during delete user
 *     }
 */
exports.manage_users_delete = async(req, res) => {
    var token = req.headers['authorization'];
    console.log(req.headers);
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, function(err, decoded) {
        if (!req.body.uuid)
            res.status(400).json({'status': 400, 'message': 'uuid key is required'});
        else {
            client_db.query("DELETE FROM users WHERE id=$1", [req.body.uuid], function(err, result) {
                if (err)
                    res.status(500).json({status: 500, message: "Error during delete user"});
                else
                    res.status(200).json({status:200, message: "User has been deleted"});
            })
        }
    });
}

/**
 * @api {get} /users/link_code Request Get User by Link Code
 * @apiName GetUser
 * @apiGroup Users
 *
 * @apiParam {String} link_code Praticien Link Code
 * @apiParam {Number} roleid (1 = Praticien, 2 = Patient)
 * 
 * @apiSuccess (200) {Object} result object.
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": link_code and roleid are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error Database
 *     }
 */
exports.get_praticien_id = async(req, res) => {
    if (!req.query.link_code || !req.query.roleid)
        res.status(400).json({'status': 400, 'message': 'link_code and roleid are required'});
    else {
        client_db.query('SELECT * FROM users WHERE roleid=$1 AND link_code=$2', [req.query.roleid, req.query.link_code], function(err, result) {
            if (result.rows[0]) {
                res.status(200).json(result.rows);
            }
            else {
                res.status(500).json({status: 500, message: err});
            }
        });
    }
}

function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}

/**
 * @api {put} /users/ Request Put User
 * @apiName PutUser
 * @apiGroup Users
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 * 
 * @apiParam {String} firstName (OPTIONAL) New user firstName.
 * @apiParam {String} lastName (OPTIONAL) New user lastName.
 * @apiParam {String} email (OPTIONAL) New user email.
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message Profil has been updated.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": No values ​​to modify were sent. You can modify (firstName, lastName, email)
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error Database
 *     }
 * @apiErrorExample {json} Error-Database-getUserperEmail:
 *     HTTP/1.1 400 User email already exists
 *     {
 *       "status": 400
 *       "message": This email xxx@xxx.xxx address is already registered
 *     }
 */
exports.manage_users_put = async(req, res) => {
    var token = req.headers['authorization'];
    console.log(req.headers);
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
        else {
            console.log(decoded.id);
            var parameters = {};
            if (req.body.firstName != undefined)
                parameters['firstName'] = req.body.firstName;
            if (req.body.lastName != undefined)
                parameters['lastName']= req.body.lastName;
            if (req.body.email != undefined) {
                client_db.query('SELECT id FROM users WHERE email=$1', [req.body.email], function(err, result) {
                        if(result.rows[0])
                            return (res.status(400).json({'status': 400, 'message': 'This email [' + req.body.email + '] address is already used'}));
                });
                parameters['email'] = req.body.email;
            }
            if (isEmptyObject(parameters)) 
                return res.status(400).json({status: 400, message: "No values ​​to modify were sent. You can modify (firstName, lastName, email)"})
            client_db.query('SELECT * FROM users WHERE id=$1', [decoded.id], function(err, result) {
                if (result.rows[0]) {
                    count = 1;
                    queryString = "";
                    queryValues = [];
                    Object.keys(parameters).forEach(function(k){
                        if (count > 1)
                            queryString = queryString + ", "; 
                        console.log(k + ' - ' + parameters[k]);
                        queryString = queryString + k + "=$" + count;
                        queryValues.push(parameters[k]);
                        count++;
                    });
                    queryValues.push(decoded.id);
                    client_db.query("UPDATE users SET " + queryString + " WHERE id=$" + queryValues.length, queryValues, function(err, result) {
                        if (result)
                            return res.status(200).json({"status": 200, "message": "Profil has been updated."}).
                        else
                            return res.status(500).json(err);
                    })
                }
                else {
                    res.status(500).json({status: 500, message: err});
                }
            });
        }
    });
}

/**
 * @api {put} /users/reset_password Request Reset_Password User
 * @apiName PutReset_password
 * @apiGroup Users
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 * @apiParam {String} password User password.
 * @apiParam {String} new_password User new_password.
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message Your password has been modified.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": password, new_password are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error Database
 *     }
 */
exports.reset_password = async(req, res) => {
    var token = req.headers['authorization'];
    if (!req.body.password || !req.body.new_password)
        return res.status(400).json({'status': 400, 'message': 'password, new_password are required'});
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
        else {
            response = await change_password(req, decoded.id);
            if (response) res.status(response["status"]).json(response);
        }
    });
}

exports.mail_send = async(req, res) => {
    if (!req.body.email || !req.body.email_token)
        return res.status(400).json({status: 400, message: "email, email_token are required"});
    host=req.get('host');
    link="http://"+req.get('host')+"/verify?email_token="+req.body.email_token;
    mailOptions={
      to : req.body.email,
      subject : "Please confirm your Email account",
      html : "Hello,<br> Please Click on the link to verify your email.<br><a href="+link+">Click here to verify</a>" 
  }
  console.log(mailOptions);
  smtpTransport.sendMail(mailOptions, function(error, response){
    if(error){
        console.log(error);
        res.status(400).json({"status": 400, "message": "Error during mail sending:" + error});
    }else{query
        console.log("Message sent: " + response.message);
        res.status(200).json({"status": 200, "message": "Mail sent:" + response.message});
    }
    });
};

/**
 * @api {Post} /notification/ Request Post Notification
 * @apiName PostNotification
 * @apiGroup Notification
 * 
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message notification token created
 */
exports.create_notification = async(req, res) => {
    var token = req.headers['authorization'];
    // console.log(req.headers);
    // if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    // jwt.verify(token, config.secret, function(err, decoded) {
    //     if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
    //     else {
    //         if (!req.body.token)
    //             return (res.status(400).json({status: 400, message: "token key is provided."}));
    //         client_db.query('INSERT INTO email_token (userID, token, type) VALUES ($1, $2, $3)', [decoded.id, req.body.token, "notification"], function(err, result) {
    //             if (err)
    //                 return res.status(500).json({"status": 500, "message": err});
    //             else {
    //                 let result = await axios.post('localhost:7070/scheduled', {token: token, pushToken: req.body.token, expoNotifToken: ""});
    //                 return res.status(200).json({"status": 200, "message": "notification token created"});
    //             }
    //         })
    //     }
    // });
}


/**
 * @api {get} /token/ Request get all active tokens
 * @apiName GetToken
 * @apiGroup Token
 *
 * @apiSuccess (200) {json} token object.
 */
exports.get_tokens = async(req, res) => {
    client_db.query("SELECT * FROM email_token", function(err, result) {
        res.status(200).json(result.rows);
    })
}

/**
 * @api {get} users/token Request get all active tokens from a specific user
 * @apiName GetUserToken
 * @apiGroup Users
 * 
 * @apiParam {String} type Token type filter.
 * @apiParam {String} uuid User uuid.
 * 
 * @apiSuccess (200) {json} token object.
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": type, uuid key are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error data request
 *     }
 */
exports.get_user_token = async(req, res) => {
    if (!req.body.type || !req.body.uuid)
        return res.status(400).json({status: 400, message: "type, uuid key are required"});
    client_db.query("SELECT * FROM email_token WHERE userID=$1 AND type=$2", [req.body.uuid, req.body.type], function(err, result) {
        if (result.rows[0])
            res.status(200).json(result.rows);
        else
            res.status(500).json({status: 500, message: "Error data request"});
    })
}

/**
 * @api {delete} users/token Request delete specific token from a specific user
 * @apiName DeleteUserToken
 * @apiGroup Users
 * 
 * @apiParam {String} type Token type filter.
 * @apiParam {String} uuid User uuid.
 * @apiParam {String} token Token that will be deleted.
 * 
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message Token has been deleted.
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": type, uuid key are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error during delete token
 *     }
 */
exports.delete_user_token = async(req, res) => {
    if (!req.body.type || !req.body.uuid || !req.body.token)
        return res.status(400).json({status: 400, message: "type, uuid, token key are required"});
    client_db.query("DELETE FROM email_token WHERE userID=$1 AND type=$2 AND token=$3", [req.body.uuid, req.body.type, req.body.token], function(err, result) {
        if (err)
            res.status(500).json({status: 500, message: "Error during delete token"});
        else
            res.status(200).json({status:200, message: "Token has been deleted"});
    })
}

exports.change_page_password = async(req, res) => {
    if (!req.body.Password)
        return res.status(400).json({status: 400, message: "password key is provided."});
    if (!req.query.token)
        return (res.status(400).json({status: 400, message: "token param is provided."}));
    client_db.query("SELECT * from email_token WHERE token=$1 AND type=$2", [req.query.token, "reset_password"], function(err, result) {
        if (result.rows[0]) {
            var userID = result.rows[0].userid;
            client_db.query("DELETE FROM email_token WHERE token=$1 AND type=$2", [req.query.token, "reset_password"], function(err, result) {
                if (err)
                    return res.status(500).json({status: 500, message: "Error during delete token"});
                else {
                    bcrypt.hash(req.body.Password, 5, function(err, pwd) {
                        client_db.query("UPDATE users SET password=$1 WHERE id=$2", [pwd, userID], function(err, result) {
                            if (result)
                                return res.status(200).json({"status": 200, "message": "Password has been changed."}).
                            else
                                return res.status(500).json(err);
                        })
                    })
                }
            })
        }
        else
            return res.status(500).json({status: 500, message: "No token has been found."});
    })
}

exports.reset_page_password = async(req, res) => {
    if (!req.query.token)
        res.status(400).json({"status": 400, "message": "Error token is provided"});
    client_db.query("SELECT * from email_token WHERE token=$1 AND type=$2", [req.query.token, "reset_password"], function(err, result) {
        if (result.rows[0]) {
                fs.readFile("api/controllers/resetpassword.html", function (error, data) {  
                    if (error) {  
                        console.log(error);  
                        res.writeHead(404);  
                        res.write('Contents you are looking are Not Found');  
                    } else {  
                        res.write(data);  
                    }  
                    res.end();  
                });
        }
        else {
            res.status(400).json({"status": 400, "message": "Error your token doesn't exist"});        
        }
    })
}

/**
 * @api {put} /users/forget_password Request Forget Password User
 * @apiName PutForget_password
 * @apiGroup Users
 *
 * @apiParam {String} email User email.
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message Your password has been modified.
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": email is required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error database
 *     }
 */
exports.forget_password = async(req, res) => {
    if (!req.body.email)
        return res.status(400).json({'status': 400, 'message': 'email is required'});
    var token;
    crypto.randomBytes(20, function(err, buf) {
        token = buf.toString('hex');
        if (token) {
            client_db.query('SELECT id FROM users WHERE email=$1', [req.body.email], function(err, result) {
                if(result.rows[0]) {
                    client_db.query('INSERT INTO email_token (userID, token, type) VALUES ($1, $2, $3)', [result.rows[0].id, token, "reset_password"], function(err, result) {
                        if (err)
                            return res.status(500).json({"status": 500, "message": err});
                        else {
                            host=req.get('host');
                            link="http://"+req.get('host')+"/reset_password?token="+token;
                            mailOptions={
                              to : req.body.email,
                              subject : "Please confirm your Email account",
                              text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +  
                                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +  
                                    link + '\n\n' +  
                                    'If you did not request this, please ignore this email and your password will remain unchanged.\n'  
                            }
                            console.log(mailOptions);
                            smtpTransport.sendMail(mailOptions, function(error, response){
                                if(error){
                                    return res.status(400).json({"status": 400, "message": "Error during mail sending:" + error});
                                }else{
                                    return res.status(200).json({"status": 200, "message": "Mail sent:" + response.message});
                                }
                            });
                        }
                    })
                // res.status(200).json({"token": token, "id": result.rows[0]});    
                }
                else
                    res.status(400).json({"status": 400, "message": "There is no user with this email."});
            })
        }
    });
    
                // return (res.status(400).json({'status': 400, 'message': 'This email [' + req.body.email + '] address is already used'}));
        // });
        // client_db.query('INSERT INTO email_token (userID, token, type) VALUES ($1, $2, $3)', [client(SELECT id from foo WHERE type='blue'), decoded.link_code], function(err, result) {
            // if (err) {
                // console.log('err');
                // return resolve({"status": 500, "message": err});
            // }
            // else {
                // console.log('result');
                // return resolve({"status": 200, "message": "This bilan with data=" + jsonStr + "has been added with this link_code=" + decoded.link_code});
            // }
        // })
    // }
}

/**
 * @api {get} /users/roleId/:id Request Get Users by roleId
 * @apiName GetUsers by roleId
 * @apiGroup Users
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 *
 * @apiParam {Number} roleId (1 (Praticien), 2 (Patient))
 * 
 * @apiSuccess (200) {json} users User object[].
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error database
 *     }
 * @apiErrorExample {json} Error-Database-getUserfromToken:
 *     HTTP/1.1 400 Error Database
 *     {
 *       "status": 400
 *       "message": No users has been found with this uuid=xxxxxxx
 *     }
 * @apiErrorExample {json} Error-RoleFromToken:
 *     HTTP/1.1 400 Error Role not authorized you must be a practitioner
 *     {
 *       "status": 400
 *       "message": You are not authorized for this action
 *     }
 */
exports.getUsers = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            if (req.params.id == 1) {
                if (decoded.roleid != 1)
                    return res.status(400).json({"status": 400, "message": "You are not authorized for this action"});
            }
            client_db.query('SELECT id, roleid, firstName, lastName, email, link_code from users WHERE roleId=$1', [req.params.id], async function(err, result) {
                if (result.rows[0]) {
                    return res.status(400).json(result.rows);
                }
                else
                    return res.status(400).json({status: 400, message: "No users has been found with this uuid=" + req.params.id})
            });
        }
    });
}

/**
 * @api {put} /users/reset_link_code Request Reset_Link_Code User
 * @apiName PutReset_link_code
 * @apiGroup Users
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 * @apiParam {String} new_link_code New link code.
 * @apiParam {String} patient_email Patient email target.
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message Link_code=KINExxxxxx has been updated for this email=x@x.x".
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": new_link_code and patient_email keys are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Error database
 *     }
 */
exports.reset_link_code = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            if (!req.body.new_link_code || !req.body.patient_email)
                res.status(400).json({'status': 400, 'message': 'new_link_code and patient_email keys are required'});
            else {
                client_db.query("UPDATE users SET link_code=$1 WHERE email=$2", [req.body.new_link_code, req.body.patient_email], function(err, result) {
                    if (result)
                        return res.status(200).json({"status": 200, "message": "Link_code=" + req.body.new_link_code + " has been updated for this email=" + req.body.patient_email + "."}).
                    else
                        return res.status(500).json(err);
                })
            }
        }
    });
}


function insert_bilan(req, decoded) {
    return new Promise((resolve) => {
        try {
            client_db.query('SELECT * from users WHERE link_code=$1 AND id=$2', [decoded.link_code, decoded.id], async function(err, result) {
                if (result.rows[0]) {
                    let patient_id = result.rows[0].id;
                    client_db.query('SELECT * from users WHERE link_code=$1 AND roleid=$2', [decoded.link_code, 1], async function(err, result) {
                        if (result.rows[0]) {
                            let praticien_id = result.rows[0].id
                            console.log(result.rows[0]);
                            jsonStr = JSON.stringify(req.body.bilan);
                            client_db.query('INSERT INTO bilans (praticienID, patientID, bilan, link_code) VALUES ($1, $2, $3, $4)', [praticien_id, patient_id, jsonStr, decoded.link_code], function(err, result) {
                                if (err) {
                                    console.log('err');
                                    return resolve({"status": 500, "message": err});
                                }
                                else {
                                    console.log('result');
                                    return resolve({"status": 200, "message": "This bilan with data=" + jsonStr + "has been added with this link_code=" + decoded.link_code});
                                }
                            })
                        }
                    })
                }
                else
                    return resolve({"status": 400, "message": "No user was found with this link_code and this uuid"});
            });
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

/**
 * @api {post} /bilan/ Request Post Bilan
 * @apiName PostBilan
 * @apiGroup Bilan
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 *
 * @apiParam {Integer} bilan User final bilan.
 * 
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message This bilan with data="*****************""has been added with this link_code=KINE******.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": bilan, link_code are provided
 *     }
 * @apiErrorExample {json} Error-Database-GetUser:
 *     HTTP/1.1 400 Error Database
 *     {
 *       "status": 400
 *       "message": No user was found with this link_code and this uuid
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 */
exports.create_bilan = async(req, res) => {
    var token = req.headers['authorization'];
    if (!req.body.bilan)
        return res.status(400).json({'status': 400, 'message': 'bilan, link_code are provided'});
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            response = await insert_bilan(req, decoded);
            if (response)
                res.status(response["status"]).json(response);
        }
    });
}

/**
 * @api {get} /user/token Request Verif user token authentification
 * @apiName GetVerifToken
 * @apiGroup Users
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {Number} auth true.
 * @apiSuccess (200) {String} message Le token d'authentification du client est bien valide.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 */
exports.verif_user_token = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else
            return res.status(200).json({ auth:true, message: "Le token d'authentification du client est bien valide."});
    });
}

function select_bilan(decoded) {
    return new Promise((resolve) => {
        try {
            client_db.query('SELECT * from bilans WHERE link_code=$1', [decoded.link_code], async function(err, result) {
                if (result) {
                    return resolve({"status": 200, "message": "get all bilan from this link_code:" + decoded.link_code, "data": result.rows});
                }
                else
                    return resolve({"status": 400, "message": "No bilans has been found with this link_code:" + decoded.link_code});
            });
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

/**
 * @api {get} /bilan/ Request Get Bilan
 * @apiName GetBilan
 * @apiGroup Bilan
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message get all bilan from this link_code:KINExxxxxx.
 * @apiSuccess (200) {String} link_code User link_code.
 * @apiSuccess (200) {Object} data All bilan linked from this User.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Database-GetBilan:
 *     HTTP/1.1 400 Error Database
 *     {
 *       "status": 400
 *       "message": No bilans has been found with this link_code
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 * @apiErrorExample {json} Error-RoleFromToken:
 *     HTTP/1.1 400 Error Role not authorized you must be a practitioner
 *     {
 *       "status": 400
 *       "message": You are not authorized for this action
 *     }
 */
exports.get_bilan = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            console.log(decoded.roleid);
            console.log(decoded);
            if (decoded.roleid != 1)
                return res.status(400).json({"status": 400, "message": "You are not authorized for this action"});
            else {
                response = await select_bilan(decoded);
                if (response)
                    res.status(response["status"]).json(response);
            }
        }
    });
}

/**
 * @api {get} /bilan/users/ Request Get Bilan by id
 * @apiName GetBilanbyID
 * @apiGroup Bilan
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message get all bilan from this id:xxxxxxxxx.
 * @apiSuccess (200) {String} link_code User link_code.
 * @apiSuccess (200) {Object} data All bilan linked from this User.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Database-GetBilanByID:
 *     HTTP/1.1 400 Error Database
 *     {
 *       "status": 400
 *       "message": No bilans has been found with this id
 *     }
 * @apiErrorExample {json} Error-RoleFromToken:
 *     HTTP/1.1 400 Error Role not authorized you must be a practitioner
 *     {
 *       "status": 400
 *       "message": You are not authorized for this action
 *     }
 */
exports.get_bilan_by_id = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            client_db.query('SELECT * from bilans WHERE praticienid=$1 OR patientid=$1', [req.query.id], async function(err, result) {
                if (result) {
                    return res.status(200).json({"status": 200, "message": "get all bilan from this id:" + req.query.id, "data": result.rows});
                }
                else
                    return res.status(400).json({"status": 400, "message": "No bilans has been found with this id"});
                });
        }
    });
}

/**
 * @api {get} /user/token Request Verif user token authentification
 * @apiName GetVerifToken
 * @apiGroup Users
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 *
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {Number} auth true.
 * @apiSuccess (200) {String} message Le token d'authentification du client est bien valide.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 */
 exports.verif_user_token = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else
            return res.status(200).json({ auth:true, message: "Le token d'authentification du client est bien valide."});
    });
}

function getRequestFormat(value) {
    return (typeof(value) == "object") ? "(" + value.toString() + ")" : value; 
}

/**
 * @api {delete} /bilan/ Request delete Bilan
 * @apiName DeleteBilan
 * @apiGroup Bilan
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 * 
 * @apiParam {json} id Bilan id (Type can be a list or an integer).
 * 
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} bilan correctly removed.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Database-GetBilanByID:
 *     HTTP/1.1 400 Error Database
 *     {
 *       "status": 400
 *       "message": No bilans has been found with this id
 *     }
 * @apiErrorExample {json} Error-RoleFromToken:
 *     HTTP/1.1 400 Error Role not authorized you must be a practitioner
 *     {
 *       "status": 400
 *       "message": You are not authorized for this action
 *     }
 */
exports.delete_bilan = async(req, res) => {
    if (!req.body.id)
        return res.status(400).json({"status": 400, "message": "id param is required."})
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            if (decoded.roleid >= 2)
                return res.status(401).json({"status": 401, "message": "Unauthorized"})
            const queryRequest = 'DELETE from bilans WHERE ' + ((typeof(req.body.id) == "object") ? "id IN " + getRequestFormat(req.body.id) : "id=" + getRequestFormat(req.body.id));
            client_db.query(queryRequest, async function(err, result) {
                if (result && result.rowCount != 0)
                    return res.status(200).json({"status": 200, "message": "bilan correctly removed"});
                else
                    return res.status(400).json({"status": 400, "message": "No bilans has been found with this id"});
            });
        }
    });
}

function insert_image(data, decoded) {
    return new Promise((resolve) => {
        try {
            console.log(data);
            client_db.query('UPDATE users SET images=$1 where id=$2', [data, decoded.id], function(err, result) {
                if (err) {
                    return resolve({"status": 500, "message": err});
                }
                else {
                    return resolve({"status": 200, "message": "image updated"});
                }
            })
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

/**
 * @api {post} /api/users/image put picture profile
 * @apiName user_image
 * @apiGroup Api
 * 
 * @apiSuccess (200) image updated.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 */
 exports.user_image_post = async(req, res) => {
    var token = req.headers['authorization'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            response = await insert_image(req.body, decoded);
            if (response)
                res.status(response["status"]).json(response);
        }
    });
    
}

// /**
//  * @api {get} /api/users Request Get all ysers
//  * @apiName GetAllUsers
//  * @apiGroup Api
//  * 
//  * @apiSuccess (200) {Object} data all users profiles.
//  * @apiErrorExample {json} Error-Database:
//  *     HTTP/1.1 500 No user found
//  *     {
//  *       "status": 500
//  *       "message": No users into the database
//  *     }
//  */
exports.getAllUsers = async(req, res) => {
    client_db.query('SELECT id, roleid, firstName, lastName, email, link_code from users', async function(err, result) {
        if (result.rows[0]) {
            return res.status(200).json(result.rows);
        }
        else
            return res.status(500).json({status: 500, message: "No users into the database"})
    });
}

/**
 * @api {get} /api/filter Request Get Filtered users
 * @apiName GetFilteredUsers
 * @apiGroup Api
 *
 * @apiSuccess (200) {Object} data all users filtered profiles.
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 No user found
 *     {
 *       "status": 500
 *       "message": No users into the database
 *     }
 */
exports.filterUsers = async(req, res) => {
    client_db.query("SELECT id, roleid, firstName, lastName, email, link_code from users WHERE firstName ILIKE '%' || $1 || '%' OR lastName ILIKE '%' || $1 || '%' OR email ILIKE '%' || $1 || '%' ORDER BY firstname", [req.query.search],  async function(err, result) {
        if (result.rows[0]) {
            return res.status(200).json(result.rows);
        }
        else
            return res.status(500).json({status: 500, message: "No users into the database"})
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////////


function insert_rdv_event(req, decoded) {
    return new Promise((resolve) => {
        try {
            var parameters = {};
            if (req.body.userid != undefined)
                parameters['userid'] = req.body.userid;
            if (req.body.notes != undefined)
                parameters['notes']= req.body.notes;
            if (req.body.date != undefined)
                parameters['date']= req.body.date;
            if (req.body.title != undefined)
                parameters['title']= req.body.title;
            count = 2;
            queryString = "(id, ";
            queryValues = [uuidv4()];
            queryDotval = "($1, ",
            Object.keys(parameters).forEach(function(k){
                if (count > 2) {
                    queryString = queryString + ", ";
                    queryDotval = queryDotval + ", ";
                }
                // console.log(k + ' - ' + parameters[k]);
                queryString = queryString + k;
                queryDotval = queryDotval + "$" + count;
                queryValues.push(parameters[k]);
                count++;
            });
            queryString = queryString + ", status"
            queryDotval = queryDotval + ", $" + count
            queryString = queryString + ", type) "
            queryDotval = queryDotval + " , $" + (count + 1) + ")"
            queryValues.push(false);
            queryValues.push("MEETING");
            console.log(queryValues)
            console.log(queryDotval)
            console.log(queryString)
            client_db.query('INSERT INTO event_rdv ' + queryString + 'VALUES ' + queryDotval, queryValues, function(err, result) {
                if (err) {
                    console.log('err');
                    return resolve({"status": 500, "message": err});
                }
                else {
                    console.log('result');
                    return resolve({"status": 201, "message": "This event has been added for this user id=" + req.body.userid});
                }
            })
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}


/**
 * @api {post} /calendar/rdv Request POST Event RDV
 * @apiName PostRDVEvent
 * @apiGroup Calendar
 *
 * @apiParam {UUID} userid user uuid.
 * @apiParam {String} date Event date.
 * @apiParam {String} notes (OPTIONAL) notes for the meeting.
 * @apiParam {String} title title for the meeting.
 * 
 * @apiSuccess (201) {Number} status 201.
 * @apiSuccess (201) {String} message This event with data=xxxx has been added for this user id=xxxxxxxxx.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": userid, date, title are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 */
exports.create_rdv_event = async(req, res) => {
    var token = req.headers['authorization'];
    if (!req.body.userid || !req.body.date || !req.body.title)
        return res.status(400).json({'status': 400, 'message': 'userid, date, title are required'});
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            response = await insert_rdv_event(req, decoded);
            if (response)
                res.status(response["status"]).json(response);
        }
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////////

function modify_rdv_event(req, decoded) {
    return new Promise((resolve) => {
        try {
            client_db.query('UPDATE event_rdv SET status=$1 where id=$2', [req.body.status, req.body.eventid], function(err, result) {
                if (err) {
                    return resolve({"status": 500, "message": err});
                }
                else {
                    return resolve({"status": 201, "message": "This event has been modified for this eventid=" + req.body.eventid + " with status=" + req.body.status});
                }
            })
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

/**
 * @api {put} /calendar/rdv Request PUT Event RDV
 * @apiName PutRDVEvent
 * @apiGroup Calendar
 *
 * @apiParam {UUID} eventid event id.
 * @apiParam {Boolean} status Event date.
 * 
 * @apiSuccess (201) {Number} status 200.
 * @apiSuccess (201) {String} message This event has been modified for this eventid=xxxxxx with status=(false,true).
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": eventid, status are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 */
exports.put_rdv_event = async(req, res) => {
    var token = req.headers['authorization'];
    if (!req.body.eventid || !req.body.status)
        return res.status(400).json({'status': 400, 'message': 'eventid, status are required'});
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            response = await modify_rdv_event(req, decoded);
            if (response)
                res.status(response["status"]).json(response);
        }
    });
}

function modify_exercice_event(req, decoded) {
    return new Promise((resolve) => {
        try {
            client_db.query('UPDATE event_exercice SET status=$1 where id=$2', [req.body.status, req.body.eventid], function(err, result) {
                if (err) {
                    return resolve({"status": 500, "message": err});
                }
                else {
                    return resolve({"status": 201, "message": "This event has been modified for this eventid=" + req.body.eventid + " with status=" + req.body.status});
                }
            })
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

/**
 * @api {put} /calendar/exercice Request PUT Event EXERCICE
 * @apiName PutexerciceEvent
 * @apiGroup Calendar
 *
 * @apiParam {UUID} eventid event id.
 * @apiParam {Boolean} status Event date.
 * 
 * @apiSuccess (201) {Number} status 200.
 * @apiSuccess (201) {String} message This event has been modified for this eventid=xxxxxx with status=(false,true).
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": eventid, status are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 */
exports.put_exercice_event = async(req, res) => {
    var token = req.headers['authorization'];
    if (!req.body.eventid || !req.body.status)
        return res.status(400).json({'status': 400, 'message': 'eventid, status are required'});
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            response = await modify_exercice_event(req, decoded);
            if (response)
                res.status(response["status"]).json(response);
        }
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////////


function insert_exercice_event(req, decoded) {
    return new Promise((resolve) => {
        try {
            var parameters = {};
            if (req.body.userid != undefined)
                parameters['userid'] = req.body.userid;
            if (req.body.notes != undefined)
                parameters['notes']= req.body.notes;
            if (req.body.date != undefined)
                parameters['date']= req.body.date;
            if (req.body.title != undefined)
                parameters['title']= req.body.title;
            if (req.body.exercice_id != undefined)
                parameters['exercice_id']= req.body.exercice_id;
            if (req.body.nbr_rep != undefined)
                parameters['nbr_rep']= req.body.nbr_rep;
            if (req.body.timer != undefined)
                parameters['timer']= req.body.timer;
            count = 2;
            queryString = "(id, ";
            queryValues = [uuidv4()];
            queryDotval = "($1, ",
            Object.keys(parameters).forEach(function(k){
                if (count > 2) {
                    queryString = queryString + ", ";
                    queryDotval = queryDotval + ", ";
                }
                queryString = queryString + k;
                queryDotval = queryDotval + "$" + count;
                queryValues.push(parameters[k]);
                count++;
            });
            queryString = queryString + ", status"
            queryDotval = queryDotval + ", $" + count
            queryString = queryString + ", type) "
            queryDotval = queryDotval + " , $" + (count + 1) + ")"
            queryValues.push(false);
            queryValues.push("EXERCICE");
            console.log(queryValues)
            console.log(queryDotval)
            console.log(queryString)
            client_db.query('INSERT INTO event_exercice ' + queryString + 'VALUES ' + queryDotval, queryValues, function(err, result) {
                if (err) {
                    console.log('err');
                    return resolve({"status": 500, "message": err});
                }
                else {
                    console.log('result');
                    return resolve({"status": 201, "message": "This event has been added for this user id=" + req.body.userid});
                }
            })
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}


/**
 * @api {post} /calendar/exercice Request POST Event EXERCICE
 * @apiName PostExerciceEvent
 * @apiGroup Calendar
 *
 * @apiParam {UUID} userid user uuid.
 * @apiParam {String} date Event date.
 * @apiParam {Number} exercice_id Exercice id.
 * @apiParam {Number} nbr_rep Number of rep.
 * @apiParam {Number} timer Timer for the exercice.
 * 
 * @apiSuccess (201) {Number} status 201.
 * @apiSuccess (201) {String} message This event with data=xxxx has been added for this user id=xxxxxxxxx.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": userid, date, exercice_id, nbr_rep, timer, title are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 */
exports.create_exercice_event = async(req, res) => {
    var token = req.headers['authorization'];
    if (!req.body.userid || !req.body.date || !req.body.exercice_id || !req.body.nbr_rep || !req.body.timer || !req.body.title)
        return res.status(400).json({'status': 400, 'message': 'userid, date, exercice_id, nbr_rep, timer, title are required'});
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            response = await insert_exercice_event(req, decoded);
            if (response)
                res.status(response["status"]).json(response);
        }
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////////


function select_event(req, decoded) {
    return new Promise((resolve) => {
        try {
            final = []
            client_db.query('SELECT * from event_rdv WHERE userid=$1', [req.query.userid], async function(err, result) {
                if (result)
                    final.push(result.rows)
                client_db.query('SELECT * from event_exercice WHERE userid=$1', [req.query.userid], async function(err, result) {
                    if (result) {
                        final.push(result.rows)
                        return (resolve({"status": 200, "message": final}))
                    }
                    else
                        return (resolve({"status": 200, "message": final}))
                });
            });
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

/**
 * @api {get} /calendar/ Request Get Event
 * @apiName GetEvent
 * @apiGroup Calendar
 * 
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 * 
 * @apiParam {String} userid User uuid.
 * 
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message get all events from this id:xxxxx, data:[Object]
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": userid is required as a param
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 */
exports.get_event = async(req, res) => {
    var token = req.headers['authorization'];
    if (!req.query.userid)
        return res.status(400).json({'status': 400, 'message': 'userid is required as a param'});
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            response = await select_event(req, decoded);
            if (response)
                res.status(response["status"]).json(response);
        }
    });
}

function delete_event(req, decoded) {
    return new Promise((resolve) => {
        try {
            if (req.body.event_type == "EXERCICE") {
                client_db.query('DELETE from event_exercice WHERE id=$1', [req.body.eventid], async function(err, result) {
                    if (err)
                        return resolve({"status": 500, "message": err});
                    else
                        return resolve({"status": 200, "message": "EXERCICE Event with id=" + req.body.eventid + " has been deleted."});
                });
            }
            else if (req.body.event_type == "MEETING"){
                client_db.query('DELETE from event_rdv WHERE id=$1', [req.body.eventid], async function(err, result) {
                    if (err)
                        return resolve({"status": 500, "message": err});
                    else
                        return resolve({"status": 200, "message": "MEETING Event with id=" + req.body.eventid + " has been deleted."});
                });
            }
            else {
                return resolve({"status": 400, "message": "Event with the type:" + req.body.event_type + " does not exist."});
            }
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}

/**
 * @api {delete} /calendar/ Request Delete Event
 * @apiName DeleteEvent
 * @apiGroup Calendar
 * 
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ"
 *     }
 * @apiParam {String} eventid Event uuid.
 * @apiParam {String} event_type Event type (MEETING, EXERCICE).
 * 
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {String} message Event with id=xxxxx has been deleted.
 * @apiErrorExample {json} Error-Token:
 *     HTTP/1.1 401 Token required
 *     {
 *       "status": 401
 *       "auth": false
 *       "message": No token provided.
 *     }
 * @apiErrorExample {json} Error-Request-Body:
 *     HTTP/1.1 400 Body params not valid
 *     {
 *       "status": 400
 *       "message": eventid, event_type keys are required
 *     }
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": Database Error
 *     }
 */
exports.remove_event = async(req, res) => {
    var token = req.headers['authorization'];
    if (!req.body.eventid || !req.body.event_type)
        return res.status(400).json({'status': 400, 'message': 'eventid, event_type keys are required'});
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    jwt.verify(token, config.secret, async function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.'});
        else {
            response = await delete_event(req, decoded);
            if (response)
                res.status(response["status"]).json(response);
        }
    });
}

/**
 * @api {get} /exercice/ Request Get Exercice
 * @apiName GetExercice
 * @apiGroup Exercice
 * 
 * @apiSuccess (200) {Number} status 200.
 * @apiSuccess (200) {Object} result All exercices availables into the database
 * @apiErrorExample {json} Error-Database:
 *     HTTP/1.1 500 Error Database
 *     {
 *       "status": 500
 *       "message": No exercices into the database
 *     }
 */
exports.get_exercice = async(req, res) => {
    client_db.query('SELECT * from exercice', async function(err, result) {
        if (result.rows[0]) {
            return res.status(200).json(result.rows);
        }
        else
            return res.status(500).json({status: 500, message: "No exercices into the database"})
    });
}

function insert_exercice_p(req) {
    return new Promise((resolve) => {
        try {
            client_db.query('INSERT INTO exercice_p (exercice_id, nbr_rep, timer) VALUES ($1, $2, $3)', [req.body.exercice_id, req.body.nbr_rep, req.body.timer], function(err, result) {
                if (err) {
                    console.log('err');
                    return resolve({"status": 500, "message": err});
                }
                else {
                    console.log('result');
                    return resolve({"status": 201, "message": "This exercice has been added"});
                }
            })
        }
        catch(e) {
            return (resolve({'status': 500, 'message': 'Internal server error (' + e + ')'}));
        }
    })
}
