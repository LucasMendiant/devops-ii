define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./controllers/doc/main.js",
    "group": "/home/godeffroy/fixwekin/wekinapi/server/api/controllers/doc/main.js",
    "groupTitle": "/home/godeffroy/fixwekin/wekinapi/server/api/controllers/doc/main.js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/api/users",
    "title": "Request Get all ysers",
    "name": "GetAllUsers",
    "group": "Api",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>all users profiles.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 No user found\n{\n  \"status\": 500\n  \"message\": No users into the database\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Api"
  },
  {
    "type": "get",
    "url": "/api/filter",
    "title": "Request Get Filtered users",
    "name": "GetFilteredUsers",
    "group": "Api",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>all users filtered profiles.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 No user found\n{\n  \"status\": 500\n  \"message\": No users into the database\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Api"
  },
  {
    "type": "post",
    "url": "/api/users/image",
    "title": "put picture profile",
    "name": "user_image",
    "group": "Api",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "optional": false,
            "field": "image",
            "description": "<p>updated.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Api"
  },
  {
    "type": "delete",
    "url": "/bilan/",
    "title": "Request delete Bilan",
    "name": "DeleteBilan",
    "group": "Bilan",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "id",
            "description": "<p>Bilan id (Type can be a list or an integer).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "bilan",
            "description": "<p>correctly removed.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-GetBilanByID:",
          "content": "HTTP/1.1 400 Error Database\n{\n  \"status\": 400\n  \"message\": No bilans has been found with this id\n}",
          "type": "json"
        },
        {
          "title": "Error-RoleFromToken:",
          "content": "HTTP/1.1 400 Error Role not authorized you must be a practitioner\n{\n  \"status\": 400\n  \"message\": You are not authorized for this action\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Bilan"
  },
  {
    "type": "get",
    "url": "/bilan/",
    "title": "Request Get Bilan",
    "name": "GetBilan",
    "group": "Bilan",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>get all bilan from this link_code:KINExxxxxx.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "link_code",
            "description": "<p>User link_code.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>All bilan linked from this User.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-GetBilan:",
          "content": "HTTP/1.1 400 Error Database\n{\n  \"status\": 400\n  \"message\": No bilans has been found with this link_code\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        },
        {
          "title": "Error-RoleFromToken:",
          "content": "HTTP/1.1 400 Error Role not authorized you must be a practitioner\n{\n  \"status\": 400\n  \"message\": You are not authorized for this action\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Bilan"
  },
  {
    "type": "get",
    "url": "/bilan/users/",
    "title": "Request Get Bilan by id",
    "name": "GetBilanbyID",
    "group": "Bilan",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>get all bilan from this id:xxxxxxxxx.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "link_code",
            "description": "<p>User link_code.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>All bilan linked from this User.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-GetBilanByID:",
          "content": "HTTP/1.1 400 Error Database\n{\n  \"status\": 400\n  \"message\": No bilans has been found with this id\n}",
          "type": "json"
        },
        {
          "title": "Error-RoleFromToken:",
          "content": "HTTP/1.1 400 Error Role not authorized you must be a practitioner\n{\n  \"status\": 400\n  \"message\": You are not authorized for this action\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Bilan"
  },
  {
    "type": "post",
    "url": "/bilan/",
    "title": "Request Post Bilan",
    "name": "PostBilan",
    "group": "Bilan",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "bilan",
            "description": "<p>User final bilan.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>This bilan with data=&quot;***********<strong><strong><strong>&quot;&quot;has been added with this link_code=KINE</strong></strong></strong>.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": bilan, link_code are provided\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-GetUser:",
          "content": "HTTP/1.1 400 Error Database\n{\n  \"status\": 400\n  \"message\": No user was found with this link_code and this uuid\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Bilan"
  },
  {
    "type": "delete",
    "url": "/calendar/",
    "title": "Request Delete Event",
    "name": "DeleteEvent",
    "group": "Calendar",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "eventid",
            "description": "<p>Event uuid.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "event_type",
            "description": "<p>Event type (MEETING, EXERCICE).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Event with id=xxxxx has been deleted.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": eventid, event_type keys are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Calendar"
  },
  {
    "type": "get",
    "url": "/calendar/",
    "title": "Request Get Event",
    "name": "GetEvent",
    "group": "Calendar",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userid",
            "description": "<p>User uuid.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>get all events from this id:xxxxx, data:[Object]</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": userid is required as a param\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Calendar"
  },
  {
    "type": "post",
    "url": "/calendar/exercice",
    "title": "Request POST Event EXERCICE",
    "name": "PostExerciceEvent",
    "group": "Calendar",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "UUID",
            "optional": false,
            "field": "userid",
            "description": "<p>user uuid.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "date",
            "description": "<p>Event date.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "exercice_id",
            "description": "<p>Exercice id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nbr_rep",
            "description": "<p>Number of rep.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "timer",
            "description": "<p>Timer for the exercice.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"201\"> <li></li> </ol>"
          },
          {
            "group": "201",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>This event with data=xxxx has been added for this user id=xxxxxxxxx.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": userid, date, exercice_id, nbr_rep, timer, title are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Calendar"
  },
  {
    "type": "post",
    "url": "/calendar/rdv",
    "title": "Request POST Event RDV",
    "name": "PostRDVEvent",
    "group": "Calendar",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "UUID",
            "optional": false,
            "field": "userid",
            "description": "<p>user uuid.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "date",
            "description": "<p>Event date.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "notes",
            "description": "<p>(OPTIONAL) notes for the meeting.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title for the meeting.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"201\"> <li></li> </ol>"
          },
          {
            "group": "201",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>This event with data=xxxx has been added for this user id=xxxxxxxxx.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": userid, date, title are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Calendar"
  },
  {
    "type": "put",
    "url": "/calendar/rdv",
    "title": "Request PUT Event RDV",
    "name": "PutRDVEvent",
    "group": "Calendar",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "UUID",
            "optional": false,
            "field": "eventid",
            "description": "<p>event id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Event date.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "201",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>This event has been modified for this eventid=xxxxxx with status=(false,true).</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": eventid, status are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Calendar"
  },
  {
    "type": "put",
    "url": "/calendar/exercice",
    "title": "Request PUT Event EXERCICE",
    "name": "PutexerciceEvent",
    "group": "Calendar",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "UUID",
            "optional": false,
            "field": "eventid",
            "description": "<p>event id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Event date.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "201",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>This event has been modified for this eventid=xxxxxx with status=(false,true).</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": eventid, status are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Database Error\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Calendar"
  },
  {
    "type": "get",
    "url": "/exercice/",
    "title": "Request Get Exercice",
    "name": "GetExercice",
    "group": "Exercice",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>All exercices availables into the database</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": No exercices into the database\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Exercice"
  },
  {
    "type": "Post",
    "url": "/notification/",
    "title": "Request Post Notification",
    "name": "PostNotification",
    "group": "Notification",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>notification token created</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Notification"
  },
  {
    "type": "get",
    "url": "/token/",
    "title": "Request get all active tokens",
    "name": "GetToken",
    "group": "Token",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "json",
            "optional": false,
            "field": "token",
            "description": "<p>object.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Token"
  },
  {
    "type": "delete",
    "url": "/users/",
    "title": "Request Delete User",
    "name": "DeleteUser",
    "group": "Users",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "UUID",
            "optional": false,
            "field": "id",
            "description": "<p>Uuid user account.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": uuid key is provided\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error during delete user\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "delete",
    "url": "users/token",
    "title": "Request delete specific token from a specific user",
    "name": "DeleteUserToken",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Token type filter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User uuid.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token that will be deleted.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Token has been deleted.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": type, uuid key are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error during delete token\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/",
    "title": "Request Get User",
    "name": "GetUser",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "UUID",
            "optional": false,
            "field": "id",
            "description": "<p>Uuid user account.</p>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "roleid",
            "description": "<p>User roleid (PRATICIEN:1, PATIENT:2).</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>User firstName.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>User lastName.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "200",
            "type": "Password",
            "optional": false,
            "field": "password",
            "description": "<p>Hashed user password.</p>"
          },
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "email_confirmed",
            "description": "<p>(True, False).</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "link_code",
            "description": "<p>User link_code.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-GetUser:",
          "content": "HTTP/1.1 400 User doesn't exist\n{\n  \"status\": 400\n  \"message\": User not found\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/link_code",
    "title": "Request Get User by Link Code",
    "name": "GetUser",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "link_code",
            "description": "<p>Praticien Link Code</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "roleid",
            "description": "<p>(1 = Praticien, 2 = Patient)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>object.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": link_code and roleid are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error Database\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "users/token",
    "title": "Request get all active tokens from a specific user",
    "name": "GetUserToken",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Token type filter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User uuid.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "json",
            "optional": false,
            "field": "token",
            "description": "<p>object.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": type, uuid key are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error data request\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/roleId/:id",
    "title": "Request Get Users by roleId",
    "name": "GetUsers_by_roleId",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "roleId",
            "description": "<p>(1 (Praticien), 2 (Patient))</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "json",
            "optional": false,
            "field": "users",
            "description": "<p>User object[].</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error database\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-getUserfromToken:",
          "content": "HTTP/1.1 400 Error Database\n{\n  \"status\": 400\n  \"message\": No users has been found with this uuid=xxxxxxx\n}",
          "type": "json"
        },
        {
          "title": "Error-RoleFromToken:",
          "content": "HTTP/1.1 400 Error Role not authorized you must be a practitioner\n{\n  \"status\": 400\n  \"message\": You are not authorized for this action\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/user/token",
    "title": "Request Verif user token authentification",
    "name": "GetVerifToken",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "auth",
            "description": "<p>true.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Le token d'authentification du client est bien valide.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/user/token",
    "title": "Request Verif user token authentification",
    "name": "GetVerifToken",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "auth",
            "description": "<p>true.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Le token d'authentification du client est bien valide.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users/login",
    "title": "Request Login User",
    "name": "PostLogin",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Successful authentication.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>exemple(eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ).</p>"
          },
          {
            "group": "200",
            "type": "Boolean",
            "optional": false,
            "field": "auth",
            "description": "<p>true</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": email, password are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-Login:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Internal server error\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-ComparePassword:",
          "content": "HTTP/1.1 400 Password not valid\n{\n  \"status\": 400\n  \"message\": This password is not valid for this email: xxx@xxx.com\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-FindUserWithEmail:",
          "content": "HTTP/1.1 400 User email not found\n{\n  \"status\": 400\n  \"message\": No user has been found with this email: xxx@xxx.com\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users/register",
    "title": "Request Register User",
    "name": "PostRegister",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>User firstname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>User lastName.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type_account",
            "description": "<p>User type_account (PRATICIEN, PATIENT).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"201\"> <li></li> </ol>"
          },
          {
            "group": "201",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The account has been created.</p>"
          },
          {
            "group": "201",
            "type": "String",
            "optional": false,
            "field": "link_code",
            "description": "<p>KINExxxxxx.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Database-getUserperEmail:",
          "content": "HTTP/1.1 400 User email already exists\n{\n  \"status\": 400\n  \"message\": This email xxx@xxx.xxx address is already registered\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-Register:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Internal server error\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": email, lastName, firstName, password, type_account are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-Register-Patient-getPractitionerEqualtoLinkCode:",
          "content": "HTTP/1.1 400 Practitioner doesn't exist\n{\n  \"status\": 400\n  \"message\": No practitioner was found using this code={link_code}\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/users/forget_password",
    "title": "Request Forget Password User",
    "name": "PutForget_password",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Your password has been modified.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": email is required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error database\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/users/reset_link_code",
    "title": "Request Reset_Link_Code User",
    "name": "PutReset_link_code",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_link_code",
            "description": "<p>New link code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "patient_email",
            "description": "<p>Patient email target.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Link_code=KINExxxxxx has been updated for this email=x@x.x&quot;.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": new_link_code and patient_email keys are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error database\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/users/reset_password",
    "title": "Request Reset_Password User",
    "name": "PutReset_password",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_password",
            "description": "<p>User new_password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Your password has been modified.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": password, new_password are required\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error Database\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/users/",
    "title": "Request Put User",
    "name": "PutUser",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NjFhOTExLWY4NjktNGMwNy1hOTc1LTcwZmYxMDY4OGQ0NCIsImlhdCI6MTU5MDg1MjIyMSwiZXhwIjoxNTkwOTM4NjIxfQ.mwenHKbUt9Isuk8BNOKCUEGGSIsNQ-ah8d7QVCVoEsQ\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>(OPTIONAL) New user firstName.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>(OPTIONAL) New user lastName.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>(OPTIONAL) New user email.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<ol start=\"200\"> <li></li> </ol>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Profil has been updated.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Token:",
          "content": "HTTP/1.1 401 Token required\n{\n  \"status\": 401\n  \"auth\": false\n  \"message\": No token provided.\n}",
          "type": "json"
        },
        {
          "title": "Error-Request-Body:",
          "content": "HTTP/1.1 400 Body params not valid\n{\n  \"status\": 400\n  \"message\": No values ​​to modify were sent. You can modify (firstName, lastName, email)\n}",
          "type": "json"
        },
        {
          "title": "Error-Database:",
          "content": "HTTP/1.1 500 Error Database\n{\n  \"status\": 500\n  \"message\": Error Database\n}",
          "type": "json"
        },
        {
          "title": "Error-Database-getUserperEmail:",
          "content": "HTTP/1.1 400 User email already exists\n{\n  \"status\": 400\n  \"message\": This email xxx@xxx.xxx address is already registered\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./controllers/Controllers.js",
    "groupTitle": "Users"
  }
] });
