var multer  = require('multer');

module.exports = function(app) {
    var APIroute = require('../controllers/Controllers');
    // API Routes
    app.route('/verifyBilan')
    .all(APIroute.verifybilanaccess)

    app.route('/users/register/captcha')
    .all(APIroute.verify_captcha)

    app.route('/users/register')
    .post(APIroute.register)

    app.route('/users/token')
    .get(APIroute.verif_user_token)

    app.route('/users/login')
    .post(APIroute.login)

    app.route('/users/token')
    .get(APIroute.verif_user_token)

    app.route('/users/')
    .get(APIroute.manage_users_get)
    .put(APIroute.manage_users_put)
    .delete(APIroute.manage_users_delete)

    app.route('/users/link_code')
    .get(APIroute.get_praticien_id)

    app.route('/users/roleId/:id')
    .get(APIroute.getUsers)

    app.route('/users/reset_password')
    .put(APIroute.reset_password)

    app.route("/files/")
    .get(APIroute.getFile)

    app.route('/users/image')
    .post(APIroute.user_image_post)

    app.route('/reset_password')
    .get(APIroute.reset_page_password)
    .post(APIroute.change_page_password)

    app.route('/users/reset_link_code')
    .put(APIroute.reset_link_code)

    app.route('/users/token')
    .get(APIroute.get_user_token)
    .delete(APIroute.delete_user_token)

    app.route('/bilan/')
    .post(APIroute.create_bilan)
    .get(APIroute.get_bilan)
    .delete(APIroute.delete_bilan)

    app.route('/bilan/users/')
    .get(APIroute.get_bilan_by_id)

    app.route('/users/forget_password')
    .put(APIroute.forget_password)

    app.route('/token/')
    .get(APIroute.get_tokens)

    app.route('/test_db/')
    .all(APIroute.test_db)

    app.route('/api/users')
    .all(APIroute.getAllUsers)

    app.route('/api/filter')
    .get(APIroute.filterUsers)

    app.route('/calendar/')
    .get(APIroute.get_event)
    .delete(APIroute.remove_event)

    app.route('/notification/')
    .post(APIroute.create_notification)

    app.route('/exercice/')
    .get(APIroute.get_exercice)

    app.route('/bugs')
    .post(APIroute.sendBug)

    app.route('/calendar/rdv')
    .post(APIroute.create_rdv_event)
    .put(APIroute.put_rdv_event)

    app.route('/calendar/exercice')
    .post(APIroute.create_exercice_event)
    .put(APIroute.put_exercice_event)
  };
