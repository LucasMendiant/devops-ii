var pg = require('pg');

module.exports = {
    connectDb: function (attemp, callback){
        pg.connect('postgres://'+ process.env.POSTGRES_USER + ':' + process.env.POSTGRES_PASSWORD + '@'+ process.env.POSTGRES_HOST + '/' + process.env.POSTGRES_DB , function(error, client, done){
        if (error){
            console.log(error);
            var timer = setInterval(function(){
                attemp -= 1;
                if (attemp == 0){
                    clearInterval(timer);
                    console.log("not connected to db");
                    callback("error");
                }
                pg.connect('postgres://'+ process.env.POSTGRES_USER + ':' + process.env.POSTGRES_PASSWORD + '@'+ process.env.POSTGRES_HOST + '/' + process.env.POSTGRES_DB , function(error, client, done){
                    if (error){
                        console.log(error);
                    }
                    else{
                        clearInterval(timer);
                        console.log("connected to db");
                        callback(client);
                    }
                });
            }, 2000);
        }
        else{
            console.log("connected to db");
            callback(client);
        }});
    }
}