DROP TABLE IF EXISTS groups;
CREATE TABLE groups(
id INT,
group_name VARCHAR(100)
);

INSERT INTO groups VALUES (0, 'Admin');
INSERT INTO groups VALUES (1, 'Praticien');
INSERT INTO groups VALUES (2, 'Patient');

DROP TABLE IF EXISTS users;
CREATE TABLE users(
    id UUID NOT NULL UNIQUE,
    roleId INT,
    firstName VARCHAR(50),
    lastName VARCHAR(50),
    email VARCHAR(100) UNIQUE,
    password VARCHAR(100),
    email_confirmed bool,
    link_code VARCHAR(80),
    images TEXT
);

DROP TABLE IF EXISTS bilans;
CREATE TABLE bilans(
    praticienID UUID NOT NULL,
    FOREIGN KEY (praticienID) REFERENCES users(id),
    patientID UUID NOT NULL,
    FOREIGN KEY (patientID) REFERENCES users(id),
    id SERIAL PRIMARY KEY,
    bilan json NOT NULL,
    link_code VARCHAR(80) NOT NULL
);

DROP TABLE IF EXISTS email_token;
CREATE TABLE email_token(
    userID UUID NOT NULL,
    FOREIGN KEY (userID) REFERENCES users(id),
    token TEXT,
    type TEXT
);

DROP TABLE IF EXISTS exercice;
CREATE TABLE exercice(
    id SERIAL primary key,
    name VARCHAR(50),
    urlYoutube TEXT,
    urlAssets TEXT
);

INSERT INTO exercice VALUES (1, 'Fléchisseur de coude', 'https://youtu.be/F6kIz9SbUB4', 'flechisseur_du_coude.mp4');
INSERT INTO exercice VALUES (2, 'Ischio-jambiers', 'https://youtu.be/222QND97PNM', 'etirement_ischio_jambiers.mp4');
INSERT INTO exercice VALUES (3, 'Quadriceps', 'https://youtu.be/ddpCSBTEn3M', 'etirement_quadriceps.mp4');
INSERT INTO exercice VALUES (4, 'Triceps', 'https://youtu.be/p0ORI6Ul8CU', 'etirement_triceps.mp4');

DROP TABLE IF EXISTS event_rdv;
CREATE TABLE event_rdv(
    id UUID NOT NULL,
    userid UUID NOT NULL,
    title TEXT NOT NULL,
    FOREIGN KEY (userid) REFERENCES users(id),
    notes TEXT,
    date TIMESTAMP,
    type TEXT,
    status BOOLEAN
);

DROP TABLE IF EXISTS event_exercice;
CREATE TABLE event_exercice(
    id UUID NOT NULL,
    userid UUID NOT NULL,
    title TEXT NOT NULL,
    FOREIGN KEY (userid) REFERENCES users(id),
    date TIMESTAMP,
    exercice_id INTEGER,
    FOREIGN KEY (exercice_id) REFERENCES exercice(id),
    nbr_rep INT,
    timer INT,
    type TEXT,
    status BOOLEAN
);





























-- // events by the client
-- client.query('USE ' + TEST_DATABASE);

-- client.query(
--     'CREATE TABLE IF NOT EXISTS ' + TEST_TABLE +
--         '(id INT(11) AUTO_INCREMENT, ' +
--         'title VARCHAR(255), ' +
--         'text TEXT, ' +
--         'created DATETIME, ' +
--         'PRIMARY KEY (id))'
-- );

-- client.query(
--     'INSERT INTO ' + TEST_TABLE + ' ' +
--         'SET title = ?, text = ?, created = ?',
--     ['super cool', 'this is a nice text', '2010-08-16 10:00:23']
-- );

-- var query = client.query(
--     'INSERT INTO ' + TEST_TABLE + ' ' +
--         'SET title = ?, text = ?, created = ?',
--     ['another entry', 'because 2 entries make a better test', '2010-08-16 12:42:15']
-- );

--     -- permission_id INT,
--     -- FOREIGN KEY (role_id) ADD CONSTRAINT FK_users REFERENCES users_role(id),
-- DROP TABLE IF EXISTS users_role;
-- CREATE TABLE users_role(
--     id INT PRIMARY KEY,
--     role_name VARCHAR(100),
--     role_descrition VARCHAR(255)
-- );

-- -- DROP TABLE IF EXISTS role_permissions;
-- -- CREATE TABLE role_permissions(
-- --     id INT PRIMARY KEY,
-- --     permission_name STRING UNIQUE
-- -- );
-- -- registeredAt DATETIME,
