import axios from 'axios';

function httpRequest(url, callback) {
  axios
    .get(url)
    .then((response) => {
      callback(response);
    })
    .catch((error) => {
      callback(error);
    });
}

export default httpRequest;
