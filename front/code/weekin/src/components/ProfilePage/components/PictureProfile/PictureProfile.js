import React from 'react';
import './PictureProfile.css';
import PropTypes from 'prop-types';

const CryptoJS = require('crypto-js');

const URL_SERVER =
  'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users/image';

class PictureProfile extends React.Component {
  constructor(props) {
    super(props);
    this.fileInput = React.createRef();
    const { image } = this.props
    this.state = {
      pp: image
    }
  }

  onChangeFile(event) {
    event.stopPropagation();
    event.preventDefault();
    const file = event.target.files[0];

    const reader =  new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      // upload picture to api
      this.sendImageToApi(reader.result);
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }

  updatePicture = (image) => {
    this.setState({pp: image})
  }

  encryptData(passphrase, data) {
    return CryptoJS.AES.encrypt(data, passphrase).toString();
  }

  sendImageToApi(imageData) {
    fetch(URL_SERVER, {
      method: 'POST',
      body: JSON.stringify({ data: this.encryptData('pictureProfile', imageData) }),
      headers: {
        Authorization: localStorage.getItem('token'),
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("DATA: " , data);
        this.updatePicture(imageData)
      });
  }

  
  render() {
    const { pp } = this.state
    // check if he already have a picture profile
    return (
      <div className="picture_profile">
        <input
          id="myInput"
          type="file"
          ref={(ref) => {this.upload = ref}}
          style={{ display: 'none' }}
          onChange={(e) => this.onChangeFile(e)}
          accept="image/*"
        />
        <button
          label="Open File"
          type="button"
          onClick={() => {
            this.upload.click();
          }}
        >
          <div className="img__wrap">
            <img className="logo-menu" alt="ProfilePicture" src={pp} />
            <div className="img__description">
              <p className="img__text">Modifier</p>
            </div>
          </div>
        </button>
      </div>
    );
  }
}

PictureProfile.propTypes = {
  image: PropTypes.string.isRequired,
};

export default PictureProfile;
