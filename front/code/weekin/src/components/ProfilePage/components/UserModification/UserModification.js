import React from 'react';
import './UserModification.css';
import { EmailPopup, PasswdPopup } from './components';

class UserModification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailseen: false,
      passwdseen: false,
    };
  }

  togglePopEmail = () => {
    this.setState({
      emailseen: !this.state.emailseen,
    });
  };

  togglePopPasswd = () => {
    this.setState({
      passwdseen: !this.state.passwdseen,
    });
  };

  render() {
    return (
      <div>
        <div className="modif-container">
          <div className="email">
            <button onClick={this.togglePopEmail}>
              {this.props.selectedLanguage.profilScreen.button_email}
            </button>
            {this.state.emailseen ? (
              <EmailPopup
                toggle={this.togglePopEmail}
                selectedLanguage={this.props.selectedLanguage}
              />
            ) : null}
          </div>
          <div className="password">
            <button onClick={this.togglePopPasswd}>
              {this.props.selectedLanguage.profilScreen.button_passwd}
            </button>
            {this.state.passwdseen ? (
              <PasswdPopup
                toggle={this.togglePopPasswd}
                selectedLanguage={this.props.selectedLanguage}
              />
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

export default UserModification;
