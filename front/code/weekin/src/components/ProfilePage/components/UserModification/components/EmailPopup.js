import React from 'react';
import './EmailPopup.css';
import axios from 'axios';

const URL_SERVER = 'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users';

class EmailPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      old: '',
      new: '',
      re_new: '',
      error: '',
    };
  }

  handleClick = () => {
    this.props.toggle();
  };

  updateSearchOld(event) {
    this.setState({ error: '' });
    this.state.old = event.target.value.substr(0, 50);
    this.setState({ old: event.target.value.substr(0, 50) });
  }

  updateSearchNew(event) {
    this.setState({ error: '' });
    this.state.new = event.target.value.substr(0, 50);
    this.setState({ new: event.target.value.substr(0, 50) });
  }

  updateSearchRe(event) {
    this.setState({ error: '' });
    this.state.re_new = event.target.value.substr(0, 50);
    this.setState({ re_new: event.target.value.substr(0, 50) });
  }

  async checkData() {
    axios.defaults.headers.common.Authorization = localStorage.getItem('token');
    const error = await axios.get(URL_SERVER).then((response) => {
      if (this.state.old.replace(/\s+/g, '') != response.data.email) {
        this.setState({ error: 'Wrong old email' });
        return true;
      }
      return false;
    });
    if (error) return;
    await axios.put(URL_SERVER, { email: this.state.new })
    .then((response) => {
      window.location.reload();
    })
    .catch((error) => {
      this.setState({ error: error.response.data.message});
    });
  }

  submitData = () => {
    if (!this.state.old.length || !this.state.new.length || !this.state.re_new.length) {
      this.setState({ error: 'Email need to be set' });
      return;
    }
    if (this.state.new.replace(/\s+/g, '') != this.state.re_new.replace(/\s+/g, '')) {
      this.setState({ error: 'New email doest not match' });
      return;
    }
    if (this.state.old.replace(/\s+/g, '') == this.state.new.replace(/\s+/g, '')) {
      this.setState({ error: 'New email need to be different from old one' });
      return;
    }
    this.checkData();
  };

  render() {
    return (
      <div>
        <div className="container">
          <div className="container2">
            <div className="popup">
              <span className="close" onClick={this.handleClick}>
                &times;{' '}
              </span>
              <p className="error">{this.state.error}</p>
              <div className="input-text">
                <label>{this.props.selectedLanguage.profilScreen.email_old}</label>
                <input
                  type="text"
                  value={this.state.search}
                  onChange={this.updateSearchOld.bind(this)}
                />
              </div>
              <div className="input-text">
                <label>{this.props.selectedLanguage.profilScreen.email_new}</label>
                <input
                  type="text"
                  value={this.state.search}
                  onChange={this.updateSearchNew.bind(this)}
                />
              </div>
              <div className="input-text">
                <label>{this.props.selectedLanguage.profilScreen.email_re}</label>
                <input
                  type="text"
                  value={this.state.search}
                  onChange={this.updateSearchRe.bind(this)}
                />
              </div>
              <div className="submit">
                <button onClick={this.submitData}>
                  {this.props.selectedLanguage.profilScreen.sumbmit}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EmailPopup;
