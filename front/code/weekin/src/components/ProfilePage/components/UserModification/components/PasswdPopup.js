import React from 'react';
import './PasswdPopup.css';
import axios from 'axios';

const URL_SERVER =
  'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users/reset_password';

class PasswdPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      old: '',
      new: '',
      re_new: '',
      error: '',
    };
  }

  handleClick = () => {
    this.props.toggle();
  };

  updateSearchOld(event) {
    this.setState({ error: '' });
    this.state.old = event.target.value.substr(0, 50);
    this.setState({ old: event.target.value.substr(0, 50) });
  }

  updateSearchNew(event) {
    this.setState({ error: '' });
    this.state.new = event.target.value.substr(0, 50);
    this.setState({ new: event.target.value.substr(0, 50) });
  }

  updateSearchRe(event) {
    this.setState({ error: '' });
    this.state.re_new = event.target.value.substr(0, 50);
    this.setState({ re_new: event.target.value.substr(0, 50) });
  }

  async checkData() {
    axios.defaults.headers.common.Authorization = localStorage.getItem('token');
    await axios
      .put(URL_SERVER, {
        password: this.state.old,
        new_password: this.state.new,
      })
      .then((response) => {
        window.location.reload();
      })
      .catch((error) => {
        this.setState({ error: error.response.data.message});
      });
  }

  submitData = () => {
    if (!this.state.old.length || !this.state.new.length || !this.state.re_new.length) {
      this.setState({ error: 'Password need to be set' });
      return;
    }
    if (this.state.new.replace(/\s+/g, '') != this.state.re_new.replace(/\s+/g, '')) {
      this.setState({ error: 'New password doest not match' });
      return;
    }
    if (this.state.old.replace(/\s+/g, '') == this.state.new.replace(/\s+/g, '')) {
      this.setState({ error: 'New password need to be different from old one' });
      return;
    }
    this.checkData();
  };

  render() {
    return (
      <div>
        <div className="container">
          <div className="container2">
            <div className="popup">
              <span className="close" onClick={this.handleClick}>
                &times;{' '}
              </span>
              <p className="error">{this.state.error}</p>
              <div className="input-text">
                <label>{this.props.selectedLanguage.profilScreen.passwd_old}</label>
                <input type="text" onChange={this.updateSearchOld.bind(this)} />
              </div>
              <div className="input-text">
                <label>{this.props.selectedLanguage.profilScreen.passwd_new}</label>
                <input type="text" onChange={this.updateSearchNew.bind(this)} />
              </div>
              <div className="input-text">
                <label>{this.props.selectedLanguage.profilScreen.passwd_re}</label>
                <input type="text" onChange={this.updateSearchRe.bind(this)} />
              </div>
              <div className="submit">
                <button onClick={this.submitData}>
                  {this.props.selectedLanguage.profilScreen.sumbmit}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PasswdPopup;
