// import React from 'react'
// import "./SideMenu.css"

// export default class SideMenu extends React.Component {
//     // constructor(props){
//     //     super(props);
//     // }

//     render = () => {
//         return(
//             <div className="col-md-3 left_col">
//                     <div className="left_col scroll-view">

//                         {/* <div className="navbar nav_title">
//                             <a href="#" className="site_title">Gentelella Alela!</a>
//                         </div> */}

//                         <div className="profile">
//                             <div className="profile_pic">
//                                 <img src="images/img.jpg" alt="..." className="img-circle profile_img" />
//                             </div>
//                             <div className="profile_info">
//                                 <span>Bienvenue,</span>
//                                 <h2>Tom Girard</h2>
//                             </div>
//                         </div>

//                         <br/>
//                         <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">

//                             <div className="menu_section">
//                                 <h3>General</h3>
//                                 <ul className="nav side-menu">
//                                     <li>
//                                         <a>
//                                             <i className="fa fa-home"></i>Home
//                                             <span className="fa fa-chevron-down"></span>
//                                         </a>
//                                         <ul className="nav child_menu">
//                                             <li><a href="index.html">Dashboard</a></li>
//                                             <li><a href="index2.html">Dashboard2</a></li>
//                                         </ul>
//                                     </li>
//                                     <li><a><i className="fa fa-edit"></i> Forms <span className="fa fa-chevron-down"></span></a>
//                                         <ul className="nav child_menu">
//                                             <li><a href="form.html">General Form</a></li>
//                                             <li><a href="form_advanced.html">Advanced Components</a></li>
//                                             <li><a href="form_validation.html">Form Validation</a></li>
//                                             <li><a href="form_wizards.html">Form Wizard</a></li>
//                                             <li><a href="form_upload.html">Form Upload</a></li>
//                                             <li><a href="form_buttons.html">Form Buttons</a></li>
//                                         </ul>
//                                     </li>
//                                     <li><a><i className="fa fa-desktop"></i> UI Elements <span className="fa fa-chevron-down"></span></a>
//                                         <ul className="nav child_menu">
//                                             <li><a href="general_elements.html">General Elements</a></li>
//                                             <li><a href="media_gallery.html">Media Gallery</a></li>
//                                             <li><a href="typography.html">Typography</a></li>
//                                             <li><a href="icons.html">Icons</a></li>
//                                             <li><a href="glyphicons.html">Glyphicons</a></li>
//                                             <li><a href="widgets.html">Widgets</a></li>
//                                             <li><a href="invoice.html">Invoice</a></li>
//                                             <li><a href="inbox.html">Inbox</a></li>
//                                             <li><a href="calendar.html">Calendar</a></li>
//                                         </ul>
//                                     </li>
//                                     <li><a><i className="fa fa-table"></i> Tables <span className="fa fa-chevron-down"></span></a>
//                                         <ul className="nav child_menu">
//                                             <li><a href="tables.html">Tables</a></li>
//                                             <li><a href="tables_dynamic.html">Table Dynamic</a></li>
//                                         </ul>
//                                     </li>
//                                     <li><a><i className="fa fa-bar-chart-o"></i> Data Presentation <span className="fa fa-chevron-down"></span></a>
//                                         <ul className="nav child_menu">
//                                             <li><a href="chartjs.html">Chart JS</a></li>
//                                             <li><a href="chartjs2.html">Chart JS2</a></li>
//                                             <li><a href="morisjs.html">Moris JS</a></li>
//                                             <li><a href="echarts.html">ECharts </a></li>
//                                             <li><a href="other_charts.html">Other Charts </a></li>
//                                         </ul>
//                                     </li>
//                                 </ul>
//                             </div>
//                             <div className="menu_section">
//                                 <h3>Live On</h3>
//                                 <ul className="nav side-menu">
//                                     <li><a><i className="fa fa-bug"></i> Additional Pages <span className="fa fa-chevron-down"></span></a>
//                                         <ul className="nav child_menu">
//                                             <li><a href="e_commerce.html">E-commerce</a></li>
//                                             <li><a href="e_commerce_backend.html">E-commerce Backend</a></li>
//                                             <li><a href="projects.html">Projects</a></li>
//                                             <li><a href="project_detail.html">Project Detail</a></li>
//                                             <li><a href="contacts.html">Contacts</a></li>
//                                             <li><a href="profile.html">Profile</a></li>
//                                             <li><a href="real_estate.html">Real Estate</a></li>

//                                         </ul>
//                                     </li>
//                                     <li><a><i className="fa fa-windows"></i> Extras <span className="fa fa-chevron-down"></span></a>
//                                         <ul className="nav child_menu">
//                                             <li><a href="page_404.html">404 Error</a></li>
//                                             <li><a href="page_500.html">500 Error</a></li>
//                                             <li><a href="coming_soon.html">Coming Soon</a></li>
//                                             <li><a href="plain_page.html">Plain Page</a></li>
//                                             <li><a href="login.html">Login Page</a></li>
//                                             <li><a href="sign_up.html">SignUp Page</a></li>
//                                             <li><a href="pricing_tables.html">Pricing Tables</a></li>

//                                         </ul>
//                                     </li>
//                                     <li><a><i className="fa fa-laptop"></i> Landing Page <span className="label label-success pull-right">Coming Soon</span></a></li>
//                                 </ul>
//                             </div>

//                         </div>

//                         <div className="sidebar-footer hidden-small">
//                             <a data-toggle="tooltip" data-placement="top" title="Settings">
//                                 <span className="glyphicon glyphicon-cog" aria-hidden="true"></span>
//                             </a>
//                             <a data-toggle="tooltip" data-placement="top" title="FullScreen">
//                                 <span className="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
//                             </a>
//                             <a data-toggle="tooltip" data-placement="top" title="Lock">
//                                 <span className="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
//                             </a>
//                             <a data-toggle="tooltip" data-placement="top" title="Logout">
//                                 <span className="glyphicon glyphicon-off" aria-hidden="true"></span>
//                             </a>
//                         </div>
//                     </div>
//                 </div>
//         )
//     }
// }

import React, { Component } from 'react';
import $ from 'jquery';
import debounce from 'react-debouncing';
import '../../ProfilePage.css';

export default class SideMenu extends Component {
  //   constructor(props){
  //     super(props);
  //   }

  // Sidebar
  initSidebar() {
    const CURRENT_URL = window.location.href.split('#')[0].split('?')[0];
    const $BODY = $('body');
    const $SIDEBAR_MENU = $('#sidebar-menu');
    const $SIDEBAR_FOOTER = $('.sidebar-footer');
    const $LEFT_COL = $('.left_col');
    const $RIGHT_COL = $('.right_col');
    const $NAV_MENU = $('.nav_menu');
    const $FOOTER = $('footer');
    const setContentHeight = function () {
      // reset height
      $RIGHT_COL.css('min-height', $(window).height());

      const bodyHeight = $BODY.outerHeight();
      const footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height();
      const leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height();
      let contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

      // normalize content
      contentHeight -= $NAV_MENU.height() + footerHeight;

      $RIGHT_COL.css('min-height', contentHeight);
    };
    $SIDEBAR_MENU.find('a').on('click', function (ev) {
      console.log('clicked - sidebar_menu');
      const $li = $(this).parent();

      if ($li.is('.active')) {
        $li.removeClass('active active-sm');
        $('ul:first', $li).slideUp(() => {
          setContentHeight();
        });
      } else {
        // prevent closing menu if we are on child menu
        if (!$li.parent().is('.child_menu')) {
          $SIDEBAR_MENU.find('li').removeClass('active active-sm');
          $SIDEBAR_MENU.find('li ul').slideUp();
        } else if ($BODY.is('.nav-sm')) {
          $li.parent().find('li').removeClass('active active-sm');
          $li.parent().find('li ul').slideUp();
        }
        $li.addClass('active');

        $('ul:first', $li).slideDown(() => {
          setContentHeight();
        });
      }
    });
    // deboucing
    $(window).bind('resize', debounce(setContentHeight, 100));

    // check active menu
    $SIDEBAR_MENU.find(`a[href="${CURRENT_URL}"]`).parent('li').addClass('current-page');

    $SIDEBAR_MENU
      .find('a')
      .filter(function () {
        return this.href == CURRENT_URL;
      })
      .parent('li')
      .addClass('current-page')
      .parents('ul')
      .slideDown(() => {
        setContentHeight();
      })
      .parent()
      .addClass('active');

    setContentHeight();

    // fixed sidebar
    if ($.fn.mCustomScrollbar) {
      $('.menu_fixed').mCustomScrollbar({
        autoHideScrollbar: true,
        theme: 'minimal',
        mouseWheel: {
          preventDefault: true,
        },
      });
    }
  }
  // /Sidebar

  componentDidMount() {
    this.initSidebar();
  }

  render() {
    return (
      <div>
        <div className="left_col scroll-view">
          {/* <div className="navbar nav_title" style={{
            border: 0
          }}> */}
          {/* <a href="/" className="site_title"><i className="fa fa-paw"/>
              <span>React Admin!</span>
            </a>  */}
        </div>
        <div className="clearfix" /> {/* menu profile quick info */}
        <div className="profile clearfix">
          <div className="profile_pic">
            <img src="images/img.jpg" alt="..." className="img-circle profile_img" />
          </div>
          <div className="profile_info">
            <span>Bienvenue,</span>
            <h2>Tom Girard</h2>
          </div>
        </div>
        <div
          className="navbar nav_title"
          style={{
            border: 0,
          }}
        >
          {/* /menu profile quick info */}
          <br />
          <br />
          {/* sidebar menu */}
          <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
            <div className="menu_section">
              <h3>General</h3>
              <ul className="nav side-menu">
                <li>
                  <a href="/#">
                    <i className="fa fa-user" />
                    Profile
                    <span className="fa fa-chevron-down" />
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">Dashboard</a>
                    </li>
                    <li>
                      <a href="/#">Dashboard1</a>
                    </li>
                    <li>
                      <a href="/#">Dashboard2</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="/#">
                    <i className="fa fa-edit" />
                    2ème Onglet
                    <span className="fa fa-chevron-down" />
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">General Form</a>
                    </li>
                    <li>
                      <a href="/#">Advanced Components</a>
                    </li>
                    <li>
                      <a href="/#">Form Validation</a>
                    </li>
                    <li>
                      <a href="/#">Form Wizard</a>
                    </li>
                    <li>
                      <a href="/#">Form Upload</a>
                    </li>
                    <li>
                      <a href="/#">Form Buttons</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="/#">
                    <i className="fa fa-desktop" />
                    3ème Onglet
                    <span className="fa fa-chevron-down" />
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">General Elements</a>
                    </li>
                    <li>
                      <a href="/#">Media Gallery</a>
                    </li>
                    <li>
                      <a href="/#">Typography</a>
                    </li>
                    <li>
                      <a href="/#">Icons</a>
                    </li>
                    <li>
                      <a href="/#">Glyphicons</a>
                    </li>
                    <li>
                      <a href="/#">Widgets</a>
                    </li>
                    <li>
                      <a href="/#">Invoice</a>
                    </li>
                    <li>
                      <a href="/#">Inbox</a>
                    </li>
                    <li>
                      <a href="/#">Calendar</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="/#">
                    <i className="fa fa-table" />
                    4ème Onglet
                    <span className="fa fa-chevron-down" />
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">Tables</a>
                    </li>
                    <li>
                      <a href="/#">Table Dynamic</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="/#">
                    <i className="fa fa-bar-chart-o" />
                    5ème Onglet
                    <span className="fa fa-chevron-down" />
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">Chart JS</a>
                    </li>
                    <li>
                      <a href="/#">Chart JS2</a>
                    </li>
                    <li>
                      <a href="/#">Moris JS</a>
                    </li>
                    <li>
                      <a href="/#">ECharts</a>
                    </li>
                    <li>
                      <a href="/#">Other Charts</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="/#">
                    <i className="fa fa-clone" />
                    6ème Onglet
                    <span className="fa fa-chevron-down" />
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">Fixed Sidebar</a>
                    </li>
                    <li>
                      <a href="/#">Fixed Footer</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className="menu_section">
              <h3>Settings</h3>
              <ul className="nav side-menu">
                <li>
                  <a href="/#">
                    <i className="fa fa-bug" />
                    Additional Pages
                    <span className="fa fa-chevron-down" />
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">E-commerce</a>
                    </li>
                    <li>
                      <a href="/#">Projects</a>
                    </li>
                    <li>
                      <a href="/#">Project Detail</a>
                    </li>
                    <li>
                      <a href="/#">Contacts</a>
                    </li>
                    <li>
                      <a href="/#">Profile</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="/#">
                    <i className="fa fa-windows" />
                    Extras
                    <span className="label label-success pull-right">Coming Soon</span>
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">403 Error</a>
                    </li>
                    <li>
                      <a href="/#">404 Error</a>
                    </li>
                    <li>
                      <a href="/#">500 Error</a>
                    </li>
                    <li>
                      <a href="/#">Plain Page</a>
                    </li>
                    <li>
                      <a href="/#">Login Page</a>
                    </li>
                    <li>
                      <a href="/#">Pricing Tables</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="/#">
                    <i className="fa fa-sitemap" />
                    Multilevel Menu
                    <span className="label label-success pull-right">Coming Soon</span>
                  </a>
                  <ul className="nav child_menu">
                    <li>
                      <a href="/#">Level One</a>
                    </li>
                    <li>
                      <a href="/#">
                        Level One
                        <span className="fa fa-chevron-down" />
                      </a>
                      <ul className="nav child_menu">
                        <li className="sub_menu">
                          <a href="/#">Level Two</a>
                        </li>
                        <li>
                          <a href="/#">Level Two</a>
                        </li>
                        <li>
                          <a href="/#">Level Two</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="/#">Level One</a>
                    </li>
                  </ul>
                </li>
                {/* <li>
                  <a href="/#"><i className="fa fa-laptop"/>
                    Landing Page
                    <span className="label label-success pull-right">Coming Soon</span>
                  </a>
                </li> */}
              </ul>
            </div>
          </div>
          {/* /sidebar menu */}
          {/* /menu footer buttons */}
          <div className="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings" href="/#">
              <span className="glyphicon glyphicon-cog" aria-hidden="true" />
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen" href="/#">
              <span className="glyphicon glyphicon-fullscreen" aria-hidden="true" />
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock" href="/#">
              <span className="glyphicon glyphicon-eye-close" aria-hidden="true" />
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="/#">
              <span className="glyphicon glyphicon-off" aria-hidden="true" />
            </a>
          </div>
          {/* /menu footer buttons */}
        </div>
      </div>
    );
  }
}
