import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { PictureProfile, UserModification } from './components';
import './ProfilePage.css';
import default_image from '../../images/add_picture.png'

const CryptoJS = require('crypto-js');

const URL_SERVER = 'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users';

class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.picture = React.createRef();
    this.state = {
      name: '',
      code: '',
      accountState: '',
      email: '',
      image: default_image,
      isLoaded: false
    };
  }

  async componentDidMount() {
    axios.defaults.headers.common.Authorization = localStorage.getItem('token');
    await axios
      .get(URL_SERVER)
      .then((response) => {
        if (response.data.images){
          this.setState({
            image: this.decryptData('pictureProfile', JSON.parse(response.data.images).data),
            name: `${response.data.firstname} ${response.data.lastname.toUpperCase()}`,
            code: response.data.link_code,
            email: response.data.email,
            accountState: response.data.email_confirmed,
            isLoaded: true
          });
        }
        else
          this.setState({
            name: `${response.data.firstname} ${response.data.lastname.toUpperCase()}`,
            code: response.data.link_code,
            email: response.data.email,
            accountState: response.data.email_confirmed,
            isLoaded: true
          });
        
      })
      .catch((error) => {
        console.log(error);
      });
  }

  decryptData = (passphrase, data) => CryptoJS.AES.decrypt(data, passphrase).toString(CryptoJS.enc.Utf8)

  render = () => {
    const { selectedLanguage } = this.props;
    const { image, name, email, code, accountState, isLoaded } = this.state;
    console.log(this.state)
    return (
      <div>
        {isLoaded &&
        <div>
          <aside className="profile-card">
            <header>
              <br />
              <PictureProfile ref={this.picture} image={image && image} />

              <h1 className="profile-name">{name}</h1>

              <h2 className="profile-subname">{selectedLanguage.profilScreen.kine}</h2>
            </header>

            <div className="profile-bio">
              <br />
              <div className="profile-item">
                <h3 className="profile-spec">{selectedLanguage.profilScreen.email}</h3>
                <h4 className="profile-data">{email}</h4>
              </div>
              <br />

              <div className="profile-item">
                <h3 className="profile-spec">{selectedLanguage.profilScreen.kCode}</h3>
                <h4 className="profile-data">{code}</h4>
              </div>
              <br />

              <div className="profile-item">
                <h3 className="profile-spec">
                  {selectedLanguage.profilScreen.checkAccount}
                </h3>
                <h4 className="profile-data">
                  {accountState
                    ? selectedLanguage.profilScreen.yes
                    : selectedLanguage.profilScreen.no}
                </h4>
              </div>
              <div className="modification">
                <UserModification selectedLanguage={selectedLanguage} />
              </div>
            </div>
          </aside>
        </div>
        }
      </div>
    );
  }
}

ProfilePage.propTypes = {
  selectedLanguage: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default ProfilePage;
