export { default as App } from './app/App';
export { default as Board } from './board/board';
export { default as Registration } from './auth/Registration';
export { default as ResetPassword } from './auth/ResetPassword';
export { default as Home } from './home/Home';
export { default as ProfilePage } from './ProfilePage/ProfilePage';
export { default as CalendarI } from './calendarI/calendarI';
export { default as NavBarComp } from './NavBar/navBar';
