import React from 'react';
import logo from '../../images/Wekin_Logo-rect.png';
import './navBar.css';
import frIcon from '../../images/fr.png';
import engIcon from '../../images/eng.png';
import deIcon from '../../images/de.png';
import espIcon from '../../images/esp.png';

class NavBarComp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      navState: this.props.nav_state,
    };
    this.burgerRef = React.createRef();
    this.navRef = React.createRef();
    this.profileRef = React.createRef();
    this.bilanRef = React.createRef();
    this.calendarRef = React.createRef();
    this.logoutRef = React.createRef();
  }

  toogleSideNav = () => {
    const navLinks = document.querySelectorAll('.nav-links li');
    this.navRef.current.classList.toggle('nav-active');
    this.burgerRef.current.classList.toggle('toggle');

    navLinks.forEach((link, index) => {
      console.log(link, index);

      if (index === 0) {
        this.profileRef.current.classList.toggle('animateProfileLink');
      } else if (index === 1) {
        this.bilanRef.current.classList.toggle('animateBilanLink');
      } else if (index === 2) {
        this.calendarRef.current.classList.toggle('animateCalendarLink');
      } else if (index === 3) {
        this.logoutRef.current.classList.toggle('animateLogoutLink');
      }
    });
  };

  render = () => (
    <div>
      {this.props.nav_state ? (
        <nav className="nav-navigation">
          <div className="nav-logo">
            <a href={this.props.refresh_link_button}>
              <img className="logo-menu" src={logo} />
            </a>
          </div>
          <ul ref={this.navRef} className="nav-links">
            <li ref={this.profileRef}>
              <a href="/profile">{this.props.selectedLanguage.navTitle.profile}</a>
            </li>
            <li ref={this.bilanRef}>
              <a href="/Bilan">{this.props.selectedLanguage.navTitle.checkup}</a>
            </li>
            <li ref={this.calendarRef}>
              <a href="/calendar">{this.props.selectedLanguage.navTitle.calendar}</a>
            </li>
            <li ref={this.logoutRef}>
              <a href="/">{this.props.selectedLanguage.navTitle.logout}</a>
            </li>
            <div className="pickLanguage">
              <a className="langIcon" onClick={() => this.props.changeLanguage('fr')}>
                <img className="delete_image" src={frIcon} />
              </a>
              <a className="langIcon" onClick={() => this.props.changeLanguage('eng')}>
                <img className="delete_image" src={engIcon} />
              </a>
              <a className="langIcon" onClick={() => this.props.changeLanguage('de')}>
                <img className="delete_image" src={deIcon} />
              </a>
              <a className="langIcon" onClick={() => this.props.changeLanguage('esp')}>
                <img className="delete_image" src={espIcon} />
              </a>
            </div>
          </ul>
          <div ref={this.burgerRef} className="burgerMenu" onClick={() => this.toogleSideNav()}>
            <div className="line1" />
            <div className="line2" />
            <div className="line3" />
          </div>
        </nav>
      ) : (
        <nav className="nav-navigation">
          <div className="nav-logo">
            <a href={this.props.refresh_link_button}>
              <img className="logo-menu" src={logo} />
            </a>
          </div>
          <div className="pickLanguage">
            <a className="langIcon" onClick={() => this.props.changeLanguage('fr')}>
              <img className="delete_image" src={frIcon} />
            </a>
            <a className="langIcon" onClick={() => this.props.changeLanguage('eng')}>
              <img className="delete_image" src={engIcon} />
            </a>
            <a className="langIcon" onClick={() => this.props.changeLanguage('de')}>
              <img className="delete_image" src={deIcon} />
            </a>
            <a className="langIcon" onClick={() => this.props.changeLanguage('esp')}>
              <img className="delete_image" src={espIcon} />
            </a>
          </div>
        </nav>
      )}
    </div>
  );
}

export default NavBarComp;
