import React from 'react';
import './DrawerButton.css';

const DrawerButton = (props) => (
  <button className="button" onClick={props.click}>
    <div className="button_ligne" />
    <div className="button_ligne" />
    <div className="button_ligne" />
  </button>
);

export default DrawerButton;
