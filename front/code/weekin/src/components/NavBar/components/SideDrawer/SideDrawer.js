import React from 'react';
import './SideDrawer.css';

const SideDrawer = (props) => {
  let drawerClass = 'side-drawer';
  if (props.show) {
    drawerClass = 'side-drawer open';
  }
  return (
    <nav className={drawerClass}>
      <ul>
        <li>
          <a href="/">Logout</a>
        </li>
        <li>
          <a href="/profile">Profil</a>
        </li>
        <li>
          <a href="/Bilan">Bilan</a>
        </li>
        <li>
          <a href="/calendar">Calendar</a>
        </li>
      </ul>
    </nav>
  );
};

export default SideDrawer;
