import AOS from 'aos';

export { default as Bilan } from './bilan/bilan';
export { default as SearchBar } from './SearchBar/SearchBar';
AOS.init();
