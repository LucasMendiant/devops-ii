import React from 'react';
import { Rating } from './components';
import './bilan.css';
import axios from 'axios';
import { Divider } from '@material-ui/core';
import AOS from 'aos';
import bin_emoticons from '../../../../images/bin_emoticon.png';
import 'aos/dist/aos.css';

const URL_SERVER = 'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/bilan';

class Bilan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked : false
    };
    this.delete = this.delete.bind(this);
    AOS.init();
  }

  delete() {
    axios.defaults.headers.common.Authorization = localStorage.getItem('token');
    axios
      .delete(URL_SERVER,
        {"data": {
          "id" : this.props.id
        }})
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setActive = () => {
    if (!this.state.checked) {
      this.state.checked = true;
      this.props.addBilanToEdit(this.props.id);
    }
  }

  setInactive = () => {
    if (this.state.checked){
      this.state.checked = false;
      this.props.removeBilanToEdit(this.props.id)
    }
  }

  editMode = () => {
    this.state.checked = ! this.state.checked
    if (this.state.checked)
      this.props.addBilanToEdit(this.props.id)
    else
      this.props.removeBilanToEdit(this.props.id)
  }

  render() {
    return (
      <div data-aos="fade-up" data-aos-delay="100" data-aos-duration="200">
        <div className="Bilan">
          <div className="description">
            <h1 className="patient_name">
              {this.props.selectedLanguage.bilanScreen.patient} {this.props.name}
            </h1>
            <h1 className="exercice_name">
              {this.props.selectedLanguage.bilanScreen.checkUp.exercise} {this.props.exercice}
            </h1>
            <h1 className="exercice_date">
              {this.props.selectedLanguage.bilanScreen.date}: {this.props.date}
            </h1>
          </div>
          <div className="Divider">
            <Divider style={{ backgroundColor: 'whitesmoke' }} />
          </div>
          <div className="rating">
            <div className="firstColumnRating">
              <div className="tired">
                <Rating
                  number={this.props.tired}
                  name={this.props.selectedLanguage.bilanScreen.tired}
                />
              </div>
              <div className="pain">
                <Rating
                  number={this.props.pain}
                  name={this.props.selectedLanguage.bilanScreen.pain}
                />
              </div>
            </div>

            <div className="secondColumnRating">
              <div className="breath">
                <Rating
                  number={this.props.breath}
                  name={this.props.selectedLanguage.bilanScreen.breath}
                />
              </div>
              <div className="difficulty">
                <Rating
                  number={this.props.difficulty}
                  name={this.props.selectedLanguage.bilanScreen.difficulty}
                />
              </div>
            </div>
          </div>
          <div className="allCommentWrap">
            <div className="allComment">
              <h1 className="commentaire">
                {this.props.selectedLanguage.bilanScreen.checkUp.comment}
              </h1>
              <Divider style={{ backgroundColor: 'black', margin: '3%' }} />
              <h1 className="info-message">{this.props.comment}</h1>
            </div>
          </div>
          <div className="delete">
            {!this.props.edit_mode ? (
              <button className="delete_button" onClick={this.delete} title="Delete bilan">
              {/* another option using emoji */}
              {/* <span aria-label="a rocket blasting off" role="img">🗑</span> */}
              <img className="delete_image" src={bin_emoticons} />
            </button>
            ) : <input
                  type="checkbox"
                  onChange={this.editMode}
                  checked={this.state.checked}
                />}
          </div>
        </div>
      </div>
    );
  }
}

export default Bilan;
