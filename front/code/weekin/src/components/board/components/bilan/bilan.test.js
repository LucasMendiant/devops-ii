import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Bilan from './bilan';

Enzyme.configure({ adapter: new Adapter() });

describe('Test case for Board', () => {
  test('message props', () => {
    const selectedLanguage = {
      navTitle: {
        profile: 'Profil',
        checkup: 'Bilan',
        calendar: 'Calendrier',
        logout: 'Déconnexion',
      },
      loginScreen: {
        welcome: 'Bienvenue sur Wekin',
        password: 'Mot de Passe',
        login: 'Connexion',
        forgotPass: 'Mot de passe oublié?',
        noAccount: 'Pas de compte?',
        resetPass: 'Réinitialiser le mot de passe',
        signUp: "S'inscrire",
      },
      registerScreen: {
        registration: 'Inscription',
        firstname: 'Prénom',
        lastname: 'Nom',
        email: 'Email',
        password: 'Mot de Passe',
        confirmPassword: 'Confirmation Mot de Passe',
        register: "S'inscrire",
      },
      resetPassScreen: {
        resetPassword: 'Réinitialisation du Mot de passe',
        email: 'Email',
      },
      profilScreen: {
        kine: 'Kinésithérapeute',
        email: 'Email:',
        kCode: 'Code KINE:',
        checkAccount: 'Vérification du compte:',
        yes: 'Oui',
        no: 'Non',
      },
      bilanScreen: {
        search: 'Recherche',
        patient: 'Patient:',
        all: 'Tous',
        sortBy: 'Trier par:',
        date: 'Date',
        tired: 'Fatigue',
        pain: 'Douleur musculaire',
        breath: 'Difficultés respiratoires',
        difficulty: 'Difficulté générale',
        checkUp: {
          exercise: 'Exercice:',
          comment: 'Commentaire',
        },
      },
    };
    const mounted = Enzyme.mount(
      <Bilan
        comment="message"
        difficulty={2}
        tiredness={2}
        execution={2}
        confort={2}
        name="test"
        exercice_name="test"
        date="10.10.2020"
        selectedLanguage={selectedLanguage}
      />
    );
    expect(mounted.find('.info-message').text()).toBe('message');
  });
});
