import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import Rating from './rating';

const default_range = 4;

Enzyme.configure({ adapter: new Adapter() });
describe('Test case for rating component', () => {
  test('test number', () => {
    const component = <Rating number={3} name="Test" />;
    expect(component.props.number).toBe(3);
  });
  test('test name', () => {
    const component = <Rating number={3} name="Test" />;
    expect(component.props.name).toBe('Test');
  });
  test('test default range', () => {
    const wrapper = Enzyme.shallow(<Rating number={3} name="Test" />);
    expect(wrapper.state('range')).toBe(default_range);
  });
});
