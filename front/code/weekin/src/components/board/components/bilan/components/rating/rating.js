import React from 'react';
import Rate from 'react-rating';
import './rating.css';

class Rating extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      range: 4,
    };
  }

  render() {
    return (
      <div className="rate">
        <h1 className="name">{this.props.name}</h1>
        <div className="rateIcons">
          <Rate
            initialRating={this.props.number}
            readonly
            stop={this.state.range}
            style={{ color: '#2DB4A0' }}
          />
        </div>
      </div>
    );
  }
}

export default Rating;
