import React from 'react';
import './SearchBar.css';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
  }

  forceUpdateSearch(data) {
    const filteredBilan = data.filter((bilan) => {
      if (bilan.bilan.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1) return 1;
      if (bilan.bilan.patient_name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1)
        return 1;
      return 0;
    });
    return filteredBilan;
  }

  updateSearch(event) {
    this.state.search = event.target.value.substr(0, 20);
    const filteredBilan = this.props.bilans.filter((bilan) => {
      if (bilan.bilan.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1) return 1;
      if (bilan.bilan.patient_name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1)
        return 1;
      return 0;
    });
    this.props.updateBilan(filteredBilan);
  }

  render() {
    return (
      <input
        type="text"
        className="SearchBar"
        value={this.state.search}
        onChange={this.updateSearch.bind(this)}
      />
    );
  }
}

export default SearchBar;
