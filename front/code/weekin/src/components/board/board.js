import React from 'react';
import axios from 'axios';
import $ from 'jquery';
import { Bilan, SearchBar } from './components';
import { NavBarComp } from '..';
import './board.css';
import bin_emoticons from '../../images/bin_emoticon.png';


const URL_SERVER = 'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/bilan';

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isError: false,
      patients_name: [],
      all_bilan: [],
      bilan_displayed: [],
      intervalId: 0,
      name_selected: 'All',
      sort_selected: 'Date',
      search: React.createRef(),
      Ascending: false,
      edit_mode : false,
      bilan_to_remove : [],
    };
  }

  componentDidMount() {
    this.startRefresh();
    this.state.intervalId = setInterval(this.startRefresh, 2000);
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  startRefresh = () => {
    axios.defaults.headers.common.Authorization = localStorage.getItem('token');
    axios
      .get(URL_SERVER)
      .then((response) => {
        var result = response.data.data.map(function(el) {
          var o = Object.assign({}, el);
          o.bilan.edit = React.createRef();
          return o;
        })
        this.setState({ all_bilan: result });
        // remove duplicate name
        const uniqueNames = [];
        const all_patient_name = result.map((e) => e.bilan.patient_name);
        $.each(all_patient_name, (i, el) => {
          if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
        });
        this.setState({ patients_name: uniqueNames });
        let new_bilan = this.state.search.current.forceUpdateSearch(result);
        new_bilan = this.applyNameFilter(new_bilan);
        new_bilan = this.applySortFilter(new_bilan);
        if (this.state.Ascending) new_bilan = new_bilan.reverse();
        this.setState({ data: new_bilan });
      })
      .catch((error) => {
        this.setState({ data: error });
        this.setState({ isError: true });
      });
  };

  renderBilan(info, id) {
    return (
      <Bilan
        comment={info.comment}
        tired={info.tired}
        pain={info.pain}
        breath={info.breath}
        difficulty={info.difficulty}
        name={info.patient_name}
        exercice={info.name}
        date={info.date}
        id={id}
        selectedLanguage={this.props.selectedLanguage}
        edit_mode={this.state.edit_mode}
        addBilanToEdit={this.addBilanToEdit}
        removeBilanToEdit={this.removeBilanToEdit}
        ref={info.edit}
      />
    );
  }

  renderBoard() {
    const { data } = this.state;
    if (this.state.isError) {
      this.state.bilan_displayed = [
        <div>
          <h1 className="error-menu">Error occured during the board loading </h1>
          <p>{`Error: ${data.message}`}</p>
        </div>,
      ];
    }
    if (data) {
      this.state.bilan_displayed = data;
    }
  }

  optionSelected = () => {
    for (var i = 0; i < this.state.all_bilan.length; i++) {
      if (this.state.bilan_displayed[i])
        this.state.bilan_displayed[i].bilan.edit.current.setInactive();
    }
    const e = document.getElementById('patientfilter');
    this.state.name_selected = e.options[e.selectedIndex].text;
    let new_bilan_list = this.applyNameFilter(null);
    new_bilan_list = this.applySortFilter(new_bilan_list);

    this.setState({ data: new_bilan_list });
  };

  applyNameFilter(data) {
    const selected = this.state.name_selected;
    if (data == null) var bilan_list = this.state.all_bilan;
    else var bilan_list = data;
    const new_bilan = bilan_list.filter((bilan) => bilan.bilan.patient_name === selected);
    if (
      (this.state.name_selected == 'Tous' ||
        this.state.name_selected == 'All' ||
        this.state.name_selected == 'Alle' ||
        this.state.name_selected == 'Todos') &&
      data == null
    )
      return this.state.all_bilan;
    if (
      (this.state.name_selected == 'Tous' ||
        this.state.name_selected == 'All' ||
        this.state.name_selected == 'Alle' ||
        this.state.name_selected == 'Todos') &&
      data != null
    )
      return data;
    return new_bilan;
  }

  updateBilanList = (new_bilan) => {
    new_bilan = this.applyNameFilter(new_bilan);
    new_bilan = this.applySortFilter(new_bilan);

    this.setState({ data: new_bilan });
  };

  sortingBySelected = () => {
    const e = document.getElementById('sortfilter');
    this.state.sort_selected = e.options[e.selectedIndex].value;
    this.setState({ data: this.applySortFilter(null) });
  };

  applySortFilter(data) {
    const selected = this.state.sort_selected;
    if (data == null) var bilan_list = this.state.data;
    else var bilan_list = data;
    const new_bilan = bilan_list.sort((a, b) => (a.bilan[selected] < b.bilan[selected] ? 1 : -1));
    if (
      (this.state.sort_selected == 'Date' ||
        this.state.sort_selected == 'Fecha' ||
        this.state.sort_selected == 'Datiert') &&
      data == null
    ) {
      return this.applyNameFilter(this.state.all_bilan);
    }
    return new_bilan;
  }

  changeOrdering = () => {
    this.setState({ Ascending: !this.state.Ascending });
    this.state.Ascending = !this.state.Ascending;
    console.log(this.state.Ascending);
  };

  setEditMode = () => {
    this.state.edit_mode = ! this.state.edit_mode
    console.log(this.state.edit_mode)
    for (var i = 0; i < this.state.bilan_displayed.length; i++) {
      if (this.state.bilan_displayed[i])
        this.state.bilan_displayed[i].bilan.edit.current.setInactive();
    }
  }

  addBilanToEdit = (id) => {
    if(this.state.bilan_to_remove.indexOf(id) === -1) {
      this.state.bilan_to_remove.push(id)
    }
  };

  removeBilanToEdit = (id) => {
    const index = this.state.bilan_to_remove.indexOf(id);
    if (index > -1) {
      this.state.bilan_to_remove.splice(index, 1);
    }
  };

  selectAllBilan = () => {
    for (var i = 0; i < this.state.bilan_displayed.length; i++) {
      this.state.bilan_displayed[i].bilan.edit.current.setActive();
    }
  }

  removeBilan = () => {
    if (!this.state.bilan_to_remove.length)
      return;
    axios.defaults.headers.common.Authorization = localStorage.getItem('token');
    axios
      .delete(URL_SERVER,
        {"data": {
          "id" : this.state.bilan_to_remove
        }})
      .then((response) => {
        var deleted = this.state.bilan_to_remove.length
        this.state.bilan_to_remove = []
        for (var i = 0; i < this.state.bilan_displayed.length; i++) {
          if (this.state.bilan_displayed[i])
            this.state.bilan_displayed[i].bilan.edit.current.setInactive();
        }
        if (deleted == this.state.bilan_displayed.length)
          this.state.name_selected = this.props.selectedLanguage.bilanScreen.all
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    let acensing_text = '';
    if (this.state.Ascending)
      acensing_text = `${this.props.selectedLanguage.bilanScreen.ascending} ↑`;
    else acensing_text = `${this.props.selectedLanguage.bilanScreen.descending} ↓`;
    
    this.renderBoard();
    return (
      <div className="Board">
        <div className="sort-search-div">
          <SearchBar
            ref={this.state.search}
            bilans={this.state.all_bilan}
            updateBilan={this.updateBilanList}
            selectedLanguage={this.props.selectedLanguage}
          />
        </div>
        <div className="filter">
          <div className="filter-name">
            {this.props.selectedLanguage.bilanScreen.patient} &nbsp;
            <select id="patientfilter" onChange={this.optionSelected}>
              <option value={this.props.selectedLanguage.bilanScreen.all}>
                {this.props.selectedLanguage.bilanScreen.all}
              </option>
              {this.state.patients_name.map((e) => (
                <option value={e}>{e}</option>
              ))}
            </select>
          </div>
          <div className="edit-mode">
          {!this.state.edit_mode ? (
            <button onClick={this.setEditMode}>{this.props.selectedLanguage.bilanScreen.edit}</button>) : null}
          </div>
          <div className="filter-sort">
            {!this.state.edit_mode ? (
              <div>{this.props.selectedLanguage.bilanScreen.sortBy} &nbsp;
              <select id="sortfilter" onChange={this.sortingBySelected}>
              <option value="Date">{this.props.selectedLanguage.bilanScreen.date}</option>
              <option value="tired">{this.props.selectedLanguage.bilanScreen.tired}</option>
              <option value="pain">{this.props.selectedLanguage.bilanScreen.pain}</option>
              <option value="breath">{this.props.selectedLanguage.bilanScreen.breath}</option>
              <option value="difficulty">
                {this.props.selectedLanguage.bilanScreen.difficulty}
              </option>
              </select>
            </div>
            ) : null}
          </div>
          <div className="filter-order">
          {!this.state.edit_mode ? (
            <button onClick={this.changeOrdering}>{acensing_text}</button>
            ) :
            <div className="delete-bilan">
              <button onClick={this.setEditMode}>{this.props.selectedLanguage.bilanScreen.end}</button>
              <button onClick={this.selectAllBilan}>{this.props.selectedLanguage.bilanScreen.select_all}</button>
              <div className="test-bilan">
                <button  onClick={this.removeBilan} title="Delete bilan">
                  <img className="delete_image" src={bin_emoticons} />
                </button>
              </div>
            </div>}
          </div>
        </div>
        <div>
          {this.state.bilan_displayed.map((b) => {
            if (this.state.isError) return b;
            // user JSON.load if is stringified
            return this.renderBilan(b.bilan, b.id);
          })}
        </div>
      </div>
    );
  }
}

export default Board;
