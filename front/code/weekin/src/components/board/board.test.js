import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Board from './board';

Enzyme.configure({ adapter: new Adapter() });

describe('Test case for Board', () => {
  let mounted;
  let wrapper;
  const selectedLanguage = {
    navTitle: {
      profile: 'Profil',
      checkup: 'Bilan',
      calendar: 'Calendrier',
      logout: 'Déconnexion',
    },
    loginScreen: {
      welcome: 'Bienvenue sur Wekin',
      password: 'Mot de Passe',
      login: 'Connexion',
      forgotPass: 'Mot de passe oublié?',
      noAccount: 'Pas de compte?',
      resetPass: 'Réinitialiser le mot de passe',
      signUp: "S'inscrire",
    },
    registerScreen: {
      registration: 'Inscription',
      firstname: 'Prénom',
      lastname: 'Nom',
      email: 'Email',
      password: 'Mot de Passe',
      confirmPassword: 'Confirmation Mot de Passe',
      register: "S'inscrire",
    },
    resetPassScreen: {
      resetPassword: 'Réinitialisation du Mot de passe',
      email: 'Email',
    },
    profilScreen: {
      kine: 'Kinésithérapeute',
      email: 'Email:',
      kCode: 'Code KINE:',
      checkAccount: 'Vérification du compte:',
      yes: 'Oui',
      no: 'Non',
    },
    bilanScreen: {
      search: 'Recherche',
      patient: 'Patient:',
      all: 'Tous',
      sortBy: 'Trier par:',
      date: 'Date',
      tiredness: 'Fatigue',
      difficulty: 'Difficulté',
      execution: 'Exécution',
      comfort: 'Confort',
      ascending: 'Ordre croissant',
      descending: 'Ordre decroissant',
      checkUp: {
        exercise: 'Exercice:',
        comment: 'Commentaire',
      },
    },
  };
  const componentDidMountSpy = jest.spyOn(Board.prototype, 'componentDidMount');
  test('without error', () => {
    mounted = Enzyme.mount(<Board selectedLanguage={selectedLanguage} />);
    return Promise.resolve(mounted)
      .then(() => mounted.update())
      .then(() => mounted.update())
      .then(() => {
        expect(mounted.state('isError')).toBeFalsy();
      });
  });
  test('check mounting component', () => {
    wrapper = Enzyme.mount(<Board selectedLanguage={selectedLanguage} />);
    expect(Board.prototype.componentDidMount).toHaveBeenCalledTimes(2);
  });
  test('check mounting component', () => {
    test = Board;
    wrapper = Enzyme.mount(<Board selectedLanguage={selectedLanguage} />);
  });
});
