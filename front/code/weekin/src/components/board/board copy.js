import React from 'react';
import { Bilan } from './components';
import { httpRequest } from '../../utils';
import logo from '../../images/Wekin_Logo-rect.png';
import './board.css';

const FAKE_URL = 'https://jsonplaceholder.typicode.com/posts';

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoaded: false,
    };
  }

  componentDidMount() {
    httpRequest(FAKE_URL, (reponse) => {
      this.setState({ data: reponse.data });
    });
  }

  renderBilan(
    id,
    title,
    message,
    difficulty,
    tiredness,
    execution,
    confort,
    name,
    exercice_name,
    date
  ) {
    return (
      <Bilan
        id={id}
        title={title}
        message={message}
        difficulty={difficulty}
        tiredness={tiredness}
        execution={execution}
        confort={confort}
        name={name}
        exercice_name={exercice_name}
        date={date}
      />
    );
  }

  render() {
    const { data } = this.state;
    return (
      <div>
        <div className="wekin-menu">
          <img className="logo-menu" src={logo} />
          <h1 className="title-menu">Board</h1>
        </div>
        <div className="Board">
          {data.map((b) =>
            this.renderBilan(
              b.id,
              b.title,
              b.body,
              getRandomInt(5),
              getRandomInt(5),
              getRandomInt(5),
              getRandomInt(5),
              'Eric Hubert',
              'Squats',
              '10.04.2020'
            )
          )}
        </div>
      </div>
    );
  }
}

export default Board;
