import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Home from './Home';

Enzyme.configure({ adapter: new Adapter() });
describe('Test case for testing login', () => {
  let wrapper;
  const selectedLanguage = {
    navTitle: {
      profile: 'Profil',
      checkup: 'Bilan',
      calendar: 'Calendrier',
      logout: 'Déconnexion',
    },
    loginScreen: {
      welcome: 'Bienvenue sur Wekin',
      password: 'Mot de Passe',
      login: 'Connexion',
      forgotPass: 'Mot de passe oublié?',
      noAccount: 'Pas de compte?',
      resetPass: 'Réinitialiser le mot de passe',
      signUp: "S'inscrire",
    },
    registerScreen: {
      registration: 'Inscription',
      firstname: 'Prénom',
      lastname: 'Nom',
      email: 'Email',
      password: 'Mot de Passe',
      confirmPassword: 'Confirmation Mot de Passe',
      register: "S'inscrire",
    },
    resetPassScreen: {
      resetPassword: 'Réinitialisation du Mot de passe',
      email: 'Email',
    },
    profilScreen: {
      kine: 'Kinésithérapeute',
      email: 'Email:',
      kCode: 'Code KINE:',
      checkAccount: 'Vérification du compte:',
      yes: 'Oui',
      no: 'Non',
    },
    bilanScreen: {
      search: 'Recherche',
      patient: 'Patient:',
      all: 'Tous',
      sortBy: 'Trier par:',
      date: 'Date',
      tiredness: 'Fatigue',
      difficulty: 'Difficulté',
      execution: 'Exécution',
      comfort: 'Confort',
      checkUp: {
        exercise: 'Exercice:',
        comment: 'Commentaire',
      },
    },
  };
  test('username check', () => {
    wrapper = Enzyme.shallow(<Home selectedLanguage={selectedLanguage} />);
    wrapper
      .find('input[type="email"]')
      .simulate('change', { target: { name: 'email', value: 'thisisatest' } });
    expect(wrapper.state('email')).toEqual('thisisatest');
  });
  it('password check', () => {
    wrapper = Enzyme.shallow(<Home selectedLanguage={selectedLanguage} />);
    wrapper
      .find('input[type="password"]')
      .simulate('change', { target: { name: 'password', value: 'thisisatest' } });
    expect(wrapper.state('password')).toEqual('thisisatest');
  });
  // it('login check wrong right data', () => {
  //   wrapper = Enzyme.shallow(<Home selectedLanguage={selectedLanguage} />);
  //   wrapper
  //     .find('input[type="email"]')
  //     .simulate('change', { target: { name: 'email', value: 'samy.gauquelin@gmail.com' } });
  //   wrapper
  //     .find('input[type="password"]')
  //     .simulate('change', { target: { name: 'password', value: 'test' } });
  //   wrapper.find('button[type="submit"]').simulate('click');
  //   expect(wrapper.state('isLoged')).toBe(false);
  // });
});
