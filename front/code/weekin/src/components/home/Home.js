import React, { Component } from 'react';
import axios from 'axios';
import $ from 'jquery';
import './Home.css';
import PropTypes from 'prop-types';

const CryptoJS = require('crypto-js');

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  encryptData = (passphrase, data) => CryptoJS.AES.encrypt(data, passphrase).toString()

  handleChange = (event) => {
    $(event.target).addClass('has-val');
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleSubmit = (event) => {
    const { email, password } = this.state;
    const { history } = this.props;
    axios
      .post(
        'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users/login',
        {
          email: this.encryptData('email', email),
          password: this.encryptData('password', password),
          type_account: 'PRATICIEN',
        }
        // {withCredentials:  true}
      )
      .then((response) => {
        if (response.data.message === 'Successful authentication') {
          localStorage.setItem('token', response.data.token);
          history.push('/profile');
        }
        console.log('conexion response token: ', response.data.token);
      })
      .catch((error) => {
        console.log(' connexion error : ', error);
      });
    event.preventDefault();
  }

  goToForgotPassword() {
    const { history } = this.props;
    history.push('/resetPassword');
  }

  goToRegister() {
    const { history } = this.props;
    history.push('/register');
  }

  render() {
    const { selectedLanguage } = this.props;
    const { email, password } = this.state;
    return (
      <div>
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100">
              <form onSubmit={this.handleSubmit} className="login100-form validate-form">
                <span className="login100-form-title" style={{ paddingBottom: 50 }}>
                  {selectedLanguage.loginScreen.welcome}
                </span>
                <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                  <input
                    className="input100"
                    type="email"
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                    required
                  />
                  <span className="focus-input100" data-placeholder="Email" />
                </div>

                <div className="wrap-input100 validate-input" data-validate="Enter password">
                  <span className="btn-show-pass">
                    <i className="zmdi zmdi-eye" />
                  </span>
                  <input
                    className="input100"
                    type="password"
                    name="password"
                    value={password}
                    onChange={this.handleChange}
                    required
                  />
                  <span
                    className="focus-input100"
                    data-placeholder={selectedLanguage.loginScreen.password}
                  />
                </div>

                <div className="container-login100-form-btn">
                  <div className="wrap-login100-form-btn">
                    <div className="login100-form-bgbtn" />
                    <button className="login100-form-btn" type="submit">
                      {selectedLanguage.loginScreen.login}
                    </button>
                  </div>
                </div>

                <div style={{ paddingTop: 60, textAlign: 'center', flexDirection: 'column' }}>
                  <span className="txt1">{selectedLanguage.loginScreen.forgotPass}</span>

                  <button type="button" className="txt2" href="#" onClick={() => this.goToForgotPassword()}>
                    {' '}
                    {selectedLanguage.loginScreen.resetPass}
                  </button>
                </div>

                <div
                  style={{
                    paddingTop: 20,
                    textAlign: 'center',
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}
                >
                  <span className="txt1">{selectedLanguage.loginScreen.noAccount}</span>
                  <button type="button" className="txt2" href="#" onClick={() => this.goToRegister()}>
                    {' '}
                    {selectedLanguage.loginScreen.signUp}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  selectedLanguage: PropTypes.objectOf(PropTypes.object).isRequired,
  history: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default Home;
