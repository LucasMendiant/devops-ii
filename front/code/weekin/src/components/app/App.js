import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Board, Registration, Home, ResetPassword, ProfilePage, CalendarI, NavBarComp } from '..';
import './App.css';

import frJson from '../../utils/language/fr.json';
import engJson from '../../utils/language/eng.json';
import deJson from '../../utils/language/de.json';
import espJson from '../../utils/language/esp.json';

class App extends Component {
  constructor() {
    super();
    this.state = {
      selectedLanguage: frJson,
    };
  }

  componentDidMount = () => {
    const savedLanguage = localStorage.getItem('language');
    switch (savedLanguage) {
      case 'fr':
        this.setState({
          selectedLanguage: frJson,
        });
        break;
      case 'eng':
        this.setState({
          selectedLanguage: engJson,
        });
        break;
      case 'de':
        this.setState({
          selectedLanguage: deJson,
        });
        break;
      case  'esp':
        this.setState({
          selectedLanguage: espJson,
        });
        break;
      default:
        break;
    }
  };

  changeLanguage = (language) => {
    localStorage.setItem('language', language);
    switch (language) {
      case 'fr':
        this.setState({
          selectedLanguage: frJson,
        });
        break;
      case 'eng':
        this.setState({
          selectedLanguage: engJson,
        });
        break;
      case 'de':
        this.setState({
          selectedLanguage: deJson,
        });
        break;
      case  'esp':
        this.setState({
          selectedLanguage: espJson,
        });
        break;
      default:
        break;
    }
  };

  render() {
    console.log(this.state);
    const { selectedLanguage } = this.state
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route
              exact
              path="/"
              render={(props) => (
                <div>
                  <NavBarComp
                    title="Home"
                    refresh_link_button="/"
                    nav_state={false}
                    selectedLanguage={selectedLanguage}
                    changeLanguage={this.changeLanguage}
                  />
                  <Home {...props} selectedLanguage={selectedLanguage} />
                </div>
              )}
            />
            <Route
              exact
              path="/register"
              render={(props) => (
                <div>
                  <NavBarComp
                    title="Registration"
                    refresh_link_button="/"
                    nav_state={false}
                    selectedLanguage={selectedLanguage}
                    changeLanguage={this.changeLanguage}
                  />
                  <Registration {...props} selectedLanguage={selectedLanguage} />
                </div>
              )}
            />
            <Route
              exact
              path="/resetPassword"
              render={(props) => (
                <div>
                  <NavBarComp
                    title="Reset"
                    refresh_link_button="/"
                    nav_state={false}
                    selectedLanguage={selectedLanguage}
                    changeLanguage={this.changeLanguage}
                  />
                  <ResetPassword {...props} selectedLanguage={selectedLanguage} />
                </div>
              )}
            />
            <Route
              exact
              path="/bilan"
              render={(props) => (
                <div>
                  <NavBarComp
                    title="Board"
                    refresh_link_button="/calendar"
                    nav_state
                    selectedLanguage={selectedLanguage}
                    changeLanguage={this.changeLanguage}
                  />
                  <Board {...props} selectedLanguage={selectedLanguage} />
                </div>
              )}
            />
            <Route
              exact
              path="/profile"
              render={(props) => (
                <div>
                  <NavBarComp
                    title="Profile"
                    refresh_link_button="/calendar"
                    nav_state
                    selectedLanguage={selectedLanguage}
                    changeLanguage={this.changeLanguage}
                  />
                  <ProfilePage {...props} selectedLanguage={selectedLanguage} />
                </div>
              )}
            />
            <Route
              exact
              path="/calendar"
              render={(props) => (
                <div>
                  <NavBarComp
                    title="Calendar"
                    refresh_link_button="/calendar"
                    nav_state
                    selectedLanguage={selectedLanguage}
                    changeLanguage={this.changeLanguage}
                  />
                  <CalendarI {...props} selectedLanguage={selectedLanguage} />
                </div>
              )}
            />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
