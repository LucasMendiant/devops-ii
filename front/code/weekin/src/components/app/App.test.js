import renderer from 'react-test-renderer';
import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
import { Registration, Home, Board, ProfilePage, CalendarI } from '..';

import frJson from '../../utils/language/fr.json';
import engJson from '../../utils/language/eng.json';
import deJson from '../../utils/language/de.json';
import espJson from '../../utils/language/esp.json';

Enzyme.configure({ adapter: new Adapter() });

const selectedLanguage = {
  navTitle: {
    profile: 'Profil',
    checkup: 'Bilan',
    calendar: 'Calendrier',
    logout: 'Déconnexion',
  },
  loginScreen: {
    welcome: 'Bienvenue sur Wekin',
    password: 'Mot de Passe',
    login: 'Connexion',
    forgotPass: 'Mot de passe oublié?',
    noAccount: 'Pas de compte?',
    resetPass: 'Réinitialiser le mot de passe',
    signUp: "S'inscrire",
  },
  registerScreen: {
    registration: 'Inscription',
    firstname: 'Prénom',
    lastname: 'Nom',
    email: 'Email',
    password: 'Mot de Passe',
    confirmPassword: 'Confirmation Mot de Passe',
    register: "S'inscrire",
  },
  resetPassScreen: {
    resetPassword: 'Réinitialisation du Mot de passe',
    email: 'Email',
  },
  profilScreen: {
    kine: 'Kinésithérapeute',
    email: 'Email:',
    kCode: 'Code KINE:',
    checkAccount: 'Vérification du compte:',
    yes: 'Oui',
    no: 'Non',
  },
  bilanScreen: {
    search: 'Recherche',
    patient: 'Patient:',
    all: 'Tous',
    sortBy: 'Trier par:',
    date: 'Date',
    tiredness: 'Fatigue',
    difficulty: 'Difficulté',
    execution: 'Exécution',
    comfort: 'Confort',
    checkUp: {
      exercise: 'Exercice:',
      comment: 'Commentaire',
    },
  },
};

describe('Some component', () => {
  it('renders App correctly', () => {
    const tree = renderer.create(<App selectedLanguage={selectedLanguage} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('renders Registration correctly', () => {
    const tree = renderer.create(<Registration selectedLanguage={selectedLanguage} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('renders Home correctly', () => {
    const tree = renderer.create(<Home selectedLanguage={selectedLanguage} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('renders Board correctly', () => {
    const tree = renderer.create(<Board selectedLanguage={selectedLanguage} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
