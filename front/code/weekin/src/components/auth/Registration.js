import React, { Component } from 'react';
import { ReCaptcha } from './components';
import axios from 'axios';
import $ from 'jquery';
import './Registration.css';
import PropTypes from 'prop-types';

const CryptoJS = require('crypto-js');

class Registration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirm_password: '',
      type_account: 'PRATICIEN',
      registrationErrors: '',
      verified: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  encryptData(passphrase, data) {
    return CryptoJS.AES.encrypt(data, passphrase).toString();
  }

  handleChange(event) {
    $(event.target).addClass('has-val');
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleSubmit(event) {
    const { firstName, lastName, email, password, type_account } = this.state;
    axios
      .post(
        'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users/register',
        {
          firstName: this.encryptData('firstName', firstName),
          lastName: this.encryptData('lastName', lastName),
          password: this.encryptData('password', password),
          email: this.encryptData('email', email),
          type_account: type_account,
        }
        // {withCredentials:  true}
      )
      .then((response) => {
        if (response.data.message === 'The account has been created') {
          localStorage.setItem('linkCode', response.data.link_code);

          this.props.history.push('/');
          window.location.reload();
        }
        console.log('registration response: ', response);
      })
      .catch((error) => {
        console.log(' registration error : ', error);
      });
    event.preventDefault();
  }

  onVerify = (recaptchaResponse) => {
    this.setState({
      verified: true,
    });
  };

  render() {
    return (
      <div>
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100">
              <form onSubmit={this.handleSubmit} className="login100-form validate-form">
                <span className="login100-form-title" style={{ paddingBottom: 50 }}>
                  {this.props.selectedLanguage.registerScreen.registration}
                </span>
                <div className="wrap-input100 validate-input">
                  <input
                    className="input100"
                    type="text"
                    name="firstName"
                    value={this.state.firstName}
                    onChange={this.handleChange}
                    required
                  />
                  <span
                    className="focus-input100"
                    data-placeholder={this.props.selectedLanguage.registerScreen.firstname}
                  />
                </div>

                <div className="wrap-input100 validate-input">
                  <input
                    className="input100"
                    type="text"
                    name="lastName"
                    value={this.state.lastName}
                    onChange={this.handleChange}
                    required
                  />
                  <span
                    className="focus-input100"
                    data-placeholder={this.props.selectedLanguage.registerScreen.lastname}
                  />
                </div>

                <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                  <input
                    className="input100"
                    type="email"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                    required
                  />
                  <span
                    className="focus-input100"
                    data-placeholder={this.props.selectedLanguage.registerScreen.email}
                  />
                </div>

                <div className="wrap-input100 validate-input">
                  <input
                    className="input100"
                    type="password"
                    name="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                    required
                  />
                  <span
                    className="focus-input100"
                    data-placeholder={this.props.selectedLanguage.registerScreen.password}
                  />
                </div>
                <div className="wrap-input100 validate-input">
                  <input
                    className="input100"
                    type="password"
                    name="confirm_password"
                    value={this.state.confirm_password}
                    onChange={this.handleChange}
                    required
                  />
                  <span
                    className="focus-input100"
                    data-placeholder={this.props.selectedLanguage.registerScreen.confirmPassword}
                  />
                </div>
                <div>
                  <ReCaptcha onVerify={this.onVerify} />
                </div>
                <div className="container-login100-form-btn">
                  <div className="wrap-login100-form-btn">
                    <div className="login100-form-bgbtn" />
                    <button className="login100-form-btn" type="submit" disabled={!this.state.verified}> 
                      {this.props.selectedLanguage.registerScreen.register}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Registration.propTypes = {
  selectedLanguage: PropTypes.object,
  history: PropTypes.object,
};

export default Registration;
