import axios from 'axios';
import React, { Component } from 'react';
import ReCaptchaV2 from 'react-google-recaptcha';

const REACT_APP_SITE_KEY = '6LeCgCIcAAAAAAAXIijLfznJ2xL6glb0fXRBHULK';
class ReCaptcha extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  handleToken = (token) => {
    const data = { token: token };
    // If URL change are made, do not forget to add azure into the recaptcha domain
    // https://www.google.com/recaptcha/admin
    axios
      .post(
        'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users/register/captcha',
        data
      )
      .then((response) => {
        if (response.data == 'Valid') this.props.onVerify();
      })
      .catch((error) => {
        console.log(`error: ${error}`);
      });
    React.setForm((currentForm) => ({ ...currentForm, token }));
  };

  handleExpire = () => {
    React.setForm((currentForm) => ({ ...currentForm, token: null }));
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <ReCaptchaV2
          sitekey={REACT_APP_SITE_KEY}
          onChange={this.handleToken}
          onExpire={this.handleExpire}
        />
      </form>
    );
  }
}

export default ReCaptcha;
