import React, { Component } from 'react';
import axios from 'axios';
import $ from 'jquery';
import PropTypes from 'prop-types';

class ResetPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      confirm_password: '',
      type_account: 'PRATICIEN',
      registrationErrors: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    $(event.target).addClass('has-val');
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleSubmit(event) {
    const { password, confirm_password } = this.state;
    axios.defaults.headers.common.Authorization = localStorage.getItem('token');

    axios
      .put(
        'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users/reset_password',
        {
          password: password,
          new_password: confirm_password,
        }
        // {withCredentials:  true}
      )
      .then((response) => {
        if (response.data.message === 'You password has been modified') {
          this.props.history.push('/');
          window.location.reload();
        }
        console.log('reset response: ', response);
      })
      .catch((error) => {
        console.log(' reset error : ', error);
      });
    event.preventDefault();
  }

  render() {
    return (
      <div>
        {/* <div className="resetpassword"> */}
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100">
              <form className="login100-form validate-form" onSubmit={this.handleSubmit}>
                <span className="login100-form-title" style={{ paddingBottom: 50 }}>
                  {this.props.selectedLanguage.resetPassScreen.resetPassword}
                </span>
                {/* <input 
                                    type="password"
                                    name="password" 
                                    placeholder="Password" 
                                    value={this.state.password} 
                                    onChange={this.handleChange} 
                                    required
                                    /> */}
                <div className="wrap-input100 validate-input">
                  <input
                    className="input100"
                    type="email"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                    required
                  />
                  <span
                    className="focus-input100"
                    data-placeholder={this.props.selectedLanguage.resetPassScreen.email}
                  />
                </div>
                <div className="container-login100-form-btn">
                  <div className="wrap-login100-form-btn">
                    <div className="login100-form-bgbtn" />
                    <button className="login100-form-btn" type="submit">
                      {' '}
                      Reset{' '}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ResetPassword.propTypes = {
  selectedLanguage: PropTypes.object,
  history: PropTypes.object,
};

export default ResetPassword;
