import React, { Component } from 'react'
import axios from 'axios'
import { CalendarModule } from './components'
import '@progress/kendo-theme-bootstrap/dist/all.css';
import {
    Scheduler,
    WeekView,
    MonthView,
} from "@progress/kendo-react-scheduler";
import moment from 'moment';

let URL_SERVER = 'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070'

class CalendarI extends Component {

    constructor(props) {
        super(props);

        this.state = {
            viewType: "Week",
            durationBarVisible: false,
            timeRangeSelectedHandling: "Enabled",
            patients_name: [],
            patient_Events: [],
            name_selected: "",
        };
    }

    getAllPatient = async () => {
        axios.defaults.headers.common['Authorization'] = localStorage.getItem("token");
        await axios.get(URL_SERVER + "/users/link_code", { params: { link_code: localStorage.getItem("linkCode"), roleid: 2 } })
            .then(response => {
                var listTemp = [];
                response.data.map(async (e) => {
                    listTemp.push([e.id, e.firstname + " " + e.lastname]);
                });
                this.setState({ patients_name: listTemp });
                // , () => { console.log("all patient : ", this.state.patients_name) })
            })
            .catch(error => {
                this.setState({ data: error, isError: true });
            })
    }

    getEventByPatientId = async (userId) => {
        var userId = localStorage.getItem("idPatient");
        axios.defaults.headers.common['Authorization'] = localStorage.getItem("token");
        await axios.get(URL_SERVER + "/calendar/", { params: { userid: userId } })
            .then(resp => {
                var listTemp = [];
                resp.data.message[0].map(event => {
                    var startdate = new Date(event.date);
                    event.end = new Date(event.date);
                    event.date = new Date(startdate.setHours(startdate.getHours() - 1));
                    listTemp.push(event)
                });
                resp.data.message[1].map(event => {
                    var startdate = new Date(event.date);
                    var enddate = new Date(event.date);
                    event.date = startdate;
                    event.end = new Date(enddate.setHours(enddate.getHours() + 1));
                    event.isAllDay = true;
                    listTemp.push(event)
                });
                this.setState({ patient_Events: listTemp });
                // , () => { console.log("patient event : ", this.state.patient_Events) }
            })
            .catch(error => {
                this.setState({ data: error, isError: true });
                this.setState({ patient_Events: [] });
            })

    }
    startRefresh = () => {
        axios.defaults.headers.common['Authorization'] = localStorage.getItem("token");
        axios.get("http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/users/")
            .then(response => {
                localStorage.setItem("linkCode", response.data.link_code)
            })
            .catch(error => {
                this.setState({ data: error });
                this.setState({ isError: true });
            })
    }

    componentDidMount() {
        this.startRefresh();
        this.getAllPatient();
        setInterval(this.startRefresh, 200000);
    }
    optionSelected = () => {
        var e = document.getElementById("patientfilter");
        if (e.selectedIndex >= 0) {
            localStorage.setItem("idPatient", e.options[e.selectedIndex].value)
            this.getEventByPatientId(e.options[e.selectedIndex].value);
        }

    }

    render() {

        return (
            <div>
                <br/><br/><br/><br/><br/>
                <div className="filter-name">
                    <select id="patientfilter" onChange={this.optionSelected}>
                        <option value="empty">Patients</option>
                        {this.state.patients_name.map(function (e) {
                            return <option key={e[0]} value={e[0]}>{e[1]}</option>;
                        })}
                    </select>
                </div>
                <CalendarModule getEvent={this.getEventByPatientId} patientsEvent={this.state.patient_Events} />
            </div>
        );
    }
}

export default CalendarI;
