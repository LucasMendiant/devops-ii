import * as React from 'react';
import '@progress/kendo-theme-bootstrap/dist/all.css';
import {
  Scheduler,
  WeekView,
  MonthView,
} from "@progress/kendo-react-scheduler";
import {
  IntlProvider,
  LocalizationProvider,
} from "@progress/kendo-react-intl";
import '@progress/kendo-date-math/tz/Europe/Paris';
import DialogEventForm from '../calendarAppointment/DialogEventForm'
import axios from 'axios';

let URL_SERVER = 'http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070'

class CalendarModule extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.patientsEvent,
      patients_name: [],
      patient_Events: [],
    };
  }

  componentDidUpdate() {
    this.setState({ data: this.props.patientsEvent })
  }

  getDeleted(id, type) {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem("token");

    axios.delete(URL_SERVER + "/calendar/", {
      "data": {
        "eventid": id,
        "event_type": type
      }
    }).then(resp => {
      this.props.getEvent(localStorage.getItem("idPatient"));
    })
      .catch(error => {
        console.log(error)
      })
  }

  handleDataChange = ({ created, updated, deleted }) => {
    if (deleted) {
      this.state.data.filter((item) => {
        deleted.find((current) => {
          if (current.id == item.id) {
            this.getDeleted(current.id, current.type);
          }
        })
      })
    }
  };


  render() {
    const modelFields = {
      id: "id",
      title: "type",
      start: "date",
      end: "end",
    };

    return (
      <div className="App">
        <Scheduler
          timezone="Europe/Paris"
          data={this.state.data}
          modelFields={modelFields}
          onDataChange={this.handleDataChange}
          form={DialogEventForm}
          editable={{
            add: true,
            remove: true,
            drag: false,
            resize: false,
            edit: true
          }}
        >
          <WeekView />
          <MonthView
            title="Month"
            selectedDateFormat="{0:M}"
            selectedShortDateFormat="{0:M}"
          />
        </Scheduler>
      </div>
    );
  }
}

export default CalendarModule;