import * as React from 'react';
import { Dialog, DialogActionsBar } from "@progress/kendo-react-dialogs";

import axios from 'axios';
import { CalendarExerciceI, CalendarAppointmentInterface } from './components'

class DialogEventForm extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            renderAuth: true,
            isAppointmentIVisible: false,
            isExerciceIVisible: false,
            patients_Exo: [],
            visible: true,
        };

        this.handleAuthUnmount = this.handleAuthUnmount.bind(this);
        this.name = "You're not registered yet ?"

    }

    handleAuthUnmount() {
        if (this.state.renderAuth === true) {
            this.setState({ renderAuth: false });
            this.name = "Are you already registered ?"
        }
        else {
            this.setState({ renderAuth: true });
            this.name = "You're not registered yet ?"
        }
    }

    getAllExercice = () => {
        axios.defaults.headers.common['Authorization'] = localStorage.getItem("token");
        axios.get("http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/exercice/")
            .then(response => {

                var uniqueNames = [];
                response.data.map((e) => {
                    uniqueNames.push([e.id, e.name]);
                });

                this.setState({ patients_Exo: uniqueNames.slice() });
            })
            .catch(error => {
                this.setState({ data: error });
                this.setState({ isError: true });
            })
    }


    componentDidMount() {
        this.getAllExercice();
        this.state.intervalId = setInterval(this.startRefresh, 200000);
    }


    state = {
        visible: true,
    };
    toggleDialog = () => {
        this.setState({
            visible: !this.state.visible,
        });
    }

    render() {
        return (
            <div>
                {this.state.visible && (
                    <Dialog 
                        title={"Évènement en cours de création"} 
                        onClose={this.toggleDialog} 
                       
                    >
                        <br />

                        <button
                            variant="outlined"
                            color="primary"
                            className="k-button"
                            onClick={() => this.setState({ isAppointmentIVisible: true, isExerciceIVisible: false })}
                        >
                            {'Rendez-vous'}
                        </button>

                        <button
                            variant="outlined"
                            color="primary"
                            className="k-button"
                            onClick={() =>
                                this.setState({ isExerciceIVisible: true, isAppointmentIVisible: false })
                                
                            }
                        >
                            {'Mise en place d\'exercices'}
                        </button>


                        {this.state.isAppointmentIVisible ? <CalendarAppointmentInterface />  : null}
                        {this.state.isExerciceIVisible ? <CalendarExerciceI exoList={this.state.patients_Exo} /> : null}
                    </Dialog>
                )}

            </div>

        );
    }
}

export default DialogEventForm;