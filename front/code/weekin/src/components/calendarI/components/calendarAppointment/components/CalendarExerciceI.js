
import * as React from 'react';
import axios from 'axios';
import { Dialog, DialogActionsBar } from "@progress/kendo-react-dialogs";
import { Form, FormElement, Field } from '@progress/kendo-react-form';
import {
    DropDownList,
    AutoComplete,
    MultiSelect,
    ComboBox,
    MultiColumnComboBox,
    DropDownTree,
} from "@progress/kendo-react-dropdowns";

import { DateTimePicker, DatePicker } from "@progress/kendo-react-dateinputs";
import { Input, TextArea, NumericTextBox } from "@progress/kendo-react-inputs";
import moment from 'moment';



class CalendarExerciceI extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            exoType: 'Exercice List',
            userid: 'Patient name',
            date: null,
            nbrOfRepetition: 0,
            timer: 0,
            title: 'exo',
            nbrOfRecurrence: 1,

        };
    }

    handleChangeDate = (event) => {
        this.setState({
            date: moment(event.target.value).format('YYYY/MM/DD HH:mm')
        })
    }

    handleChangeNbrOfRepetition = (event) => {
        this.setState({
            nbrOfRepetition: event.target.value
        })
    }

    handleChangeNbrOfRecurrence = (event) => {
        this.setState({
            nbrOfRecurrence: event.target.value
        })
    }

    handleChangeTimer = (event) => {
        this.setState({
            timer: event.target.value
        })
    }

    sendEvent = (date) => {
        axios.post("http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/calendar/exercice", {

            userid: localStorage.getItem("idPatient"),
            date: date,
            title: this.state.title,
            exercice_id: this.state.exoType,
            nbr_rep: this.state.nbrOfRepetition,
            timer: this.state.timer,
        },
        ).catch(error => {
            console.log(" rdv registration error : ", error);
        })
    }


    handleSubmit = () => {
        this.sendEvent(this.state.date)
        if (this.state.repeat) {
            var startdate = new Date(this.state.date);
            for (var i = 1; i < this.state.nbrOfRecurrence; i++) {
                if (document.getElementsByClassName("recurrence")[0].innerHTML == "Quotidien") {
                    this.sendEvent(moment(new Date(startdate.setHours(startdate.getHours() + 24))).format('YYYY/MM/DD HH:mm'));
                }
                else if (document.getElementsByClassName("recurrence")[0].innerHTML == "Hébdomadaire") {
                    this.sendEvent(moment(new Date(startdate.setHours(startdate.getHours() + 24 * 7))).format('YYYY/MM/DD HH:mm'));
                }
            }
        }
        window.location.reload(false);
    }
    displayRecurrence = (id) => {
        console.log("on click", id)
        if (id =="dailyButton") {
            document.getElementById(id).className = "k-button k-state-selected recurrence";
            document.getElementById("weeklyButton").className = "k-button";
            document.getElementById("neverButton").className = "k-button";
            this.setState({repeat : true})
        } else if (id =="weeklyButton") {
            document.getElementById(id).className = "k-button k-state-selected recurrence";
            document.getElementById("dailyButton").className = "k-button";
            document.getElementById("neverButton").className = "k-button";
            this.setState({repeat : true})
        } else {
            document.getElementById(id).className = "k-button k-state-selected recurrence";
            document.getElementById("dailyButton").className = "k-button";
            document.getElementById("weeklyButton").className = "k-button";
            this.setState({repeat : false})
        }
       
    }

    optionSelected = () => {
        var e = document.getElementById("exercicefilter");
        if (e.selectedIndex > 0) {
            this.state.exoType = e.options[e.selectedIndex].value
        }
    }

    render() {
        return (
                <Form
                    onSubmit={this.handleSubmit}
                    render={(formRenderProps) => (
                        <FormElement
                            style={{
                                width: 400,
                            }}
                        >
                            <fieldset className={"k-form-fieldset"}>
                                <div className="mb-3">
                                    <select id="exercicefilter" onChange={this.optionSelected}>
                                        <option value="empty"> Liste des Exercices</option>
                                        {this.props.exoList.map(function (e) {
                                            return <option key={e[0]} value={e[0]}>{e[1]}</option>;
                                        })}
                                    </select>
                                </div>
                                <div className="mb-3">
                                    <Field
                                        id={"appointment"}
                                        name={"appointment"}
                                        label={"Date et heure du prochain rendez-vous"}
                                        format={"dd-MMM-yyyy HH:mm"}
                                        component={DateTimePicker}
                                        onChange={this.handleChangeDate}
                                    />
                                </div>
                                <div className="mb-3">
                                    <Field
                                        label={"Nombre de répétitions"}
                                        component={NumericTextBox}
                                        defaultValue={0}
                                        min={0}
                                        onChange={this.handleChangeNbrOfRepetition} />
                                </div>
                                <div className="mb-3">
                                    <Field
                                        label={"Chronomètre en seconde"}
                                        component={NumericTextBox}
                                        defaultValue={0}
                                        min={0}
                                        onChange={this.handleChangeTimer} />
                                </div>
                                <div  className ="k-form-field">
                                    <label  className ="k-label">Répétition</label>
                                    <div  className ="k-form-field-wrap">
                                        <div  className ="k-button-group k-button-group-stretched" role="group">
                                            <button  className ="k-button k-state-selected k-group-start"   id="neverButton"  type ="button"  onClick={() => this.displayRecurrence("neverButton")}>Jamais</button>
                                            <button  className ="k-button"  type ="button" id="dailyButton" onClick={() => this.displayRecurrence("dailyButton")} >Quotidien</button>
                                            <button  className ="k-button"  type ="button" id="weeklyButton"  onClick={() => this.displayRecurrence("weeklyButton")} >Hébdomadaire</button>
                                        </div>
                                    </div>
                                </div>
                                {this.state.repeat ? (
                                    <Field
                                    label={"Nombre de répétitions"}
                                    component={NumericTextBox}
                                    defaultValue={1}
                                    min={1}
                                    onChange={this.handleChangeNbrOfRecurrence}
                                    />
                                ) : null}
                            </fieldset>
                            <div className="k-form-buttons">
                                <button
                                    type={"submit"}
                                    className="k-button"
                                    disabled={!formRenderProps.allowSubmit}
                                >
                                    Ajouter
                                </button>
                            </div>
                        </FormElement>
                    )}
                />
        );
    }
}

export default CalendarExerciceI;


// import {
//     SchedulerFieldsContext,
//     SchedulerForm,
//   } from "@progress/kendo-react-scheduler";

//   class CalendarExerciceI extends React.Component {
//     requiredValidator = (value) => (!value ? "Field is required." : undefined);
//     descriptionLengthValidator = (description) => {
//       return !description || description.length < 40
//         ? "The description should be at least 40 characters."
//         : undefined;
//     };
  
//     render() {
//       return (
//         <SchedulerFieldsContext.Consumer>
//           {(fields) => (
//             <SchedulerForm
//               {...this.props}
//               validator={(_dataItem, formValueGetter) => {
//                 let result = {};
//                 result[fields.description] = [
//                   this.requiredValidator(formValueGetter(fields.description)),
//                   this.descriptionLengthValidator(
//                     formValueGetter(fields.description)
//                   ),
//                 ]
//                   .filter(Boolean)
//                   .reduce((current, acc) => current || acc, "");
//                 return result;
//               }}
//             />
//           )}
//         </SchedulerFieldsContext.Consumer>
//       );
//     }
//   }
//   export default CalendarExerciceI;