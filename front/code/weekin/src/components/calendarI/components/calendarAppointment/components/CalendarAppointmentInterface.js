
import * as React from 'react';
import axios from 'axios';
import { Dialog, DialogActionsBar } from "@progress/kendo-react-dialogs";
import { Form, FormElement, Field } from '@progress/kendo-react-form';
import {
    DropDownList,
    AutoComplete,
    MultiSelect,
    ComboBox,
    MultiColumnComboBox,
    DropDownTree,
} from "@progress/kendo-react-dropdowns";
import { DateTimePicker, DatePicker } from "@progress/kendo-react-dateinputs";
import {
    Checkbox,
    Input,
    NumericTextBox,
    Switch,
} from "@progress/kendo-react-inputs";
import moment from 'moment';
import { NoEncryption } from '@material-ui/icons';


 class CalendarAppointmentInterface extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            userid: 'Patient name',
            date: null,
            title: 'rdv',
            visible: true,
            repeat : false,
            nbrOfRepetition: 1,
        };

    }

    handleChangeDate = (event) => {
        this.setState({
            date: moment(event.target.value).format('YYYY/MM/DD HH:mm')
        })
    }

    handleChangeNbrOfRepetition = (event) => {
        this.setState({
            nbrOfRepetition: event.target.value
        })
    }

    sendEvent = (date) => {
        axios.post("http://x2022wekin3254678814000.francecentral.cloudapp.azure.com:7070/calendar/rdv", {

            userid: localStorage.getItem("idPatient"),
            date: date,
            title: this.state.title,
        },
        ).catch(error => {
            console.log(" rdv registration error : ", error);
        })
    }


    handleSubmit = () => {
        this.sendEvent(this.state.date)
        if (this.state.repeat) {
            var startdate = new Date(this.state.date);
            for (var i = 1; i < this.state.nbrOfRepetition; i++) {
                if (document.getElementsByClassName("recurrence")[0].innerHTML == "Quotidien") {
                    this.sendEvent(moment(new Date(startdate.setHours(startdate.getHours() + 24))).format('YYYY/MM/DD HH:mm'));
                }
                else if (document.getElementsByClassName("recurrence")[0].innerHTML == "Hébdomadaire") {
                    this.sendEvent(moment(new Date(startdate.setHours(startdate.getHours() + 24 * 7))).format('YYYY/MM/DD HH:mm'));
                }
            }
        }
        window.location.reload(false);
    }
    displayRecurrence = (id) => {
        console.log("on click", id)
        if (id =="dailyButton") {
            document.getElementById(id).className = "k-button k-state-selected recurrence";
            document.getElementById("weeklyButton").className = "k-button";
            document.getElementById("neverButton").className = "k-button";
            this.setState({repeat : true})
        } else if (id =="weeklyButton") {
            document.getElementById(id).className = "k-button k-state-selected recurrence";
            document.getElementById("dailyButton").className = "k-button";
            document.getElementById("neverButton").className = "k-button";
            this.setState({repeat : true})
        } else {
            document.getElementById(id).className = "k-button k-state-selected recurrence";
            document.getElementById("dailyButton").className = "k-button";
            document.getElementById("weeklyButton").className = "k-button";
            this.setState({repeat : false})
        }
       
    }

    render() {
        
        return (
            <div>
                <Form
                    onSubmit={this.handleSubmit}
                    render={(formRenderProps) => (
                        <FormElement
                            style={{
                                width: 400,
                            }}
                        >
                            <fieldset className={"k-form-fieldset"} >
                                <div className="k-form-field">
                                    <Field
                                        id={"appointment"}
                                        name={"appointment"}
                                        label={"Date et heure du prochain rendez-vous"}
                                        format={"dd-MMM-yyyy HH:mm"}
                                        component={DateTimePicker}
                                        onChange={this.handleChangeDate}
                                    />
                                </div>
                                 <div  className ="k-form-field">
                                    <label  className ="k-label">Répétition</label>
                                    <div  className ="k-form-field-wrap">
                                        <div  className ="k-button-group k-button-group-stretched" role="group">
                                            <button  className ="k-button k-state-selected k-group-start"   id="neverButton"  type ="button"  onClick={() => this.displayRecurrence("neverButton")}>Jamais</button>
                                            <button  className ="k-button"  type ="button" id="dailyButton" onClick={() => this.displayRecurrence("dailyButton")} >Quotidien</button>
                                            <button  className ="k-button"  type ="button" id="weeklyButton"  onClick={() => this.displayRecurrence("weeklyButton")} >Hébdomadaire</button>
                                        </div>
                                    </div>
                                </div>
                                {this.state.repeat ? (
                                    <Field
                                    label={"Nombre de répétitions"}
                                    component={NumericTextBox}
                                    defaultValue={1}
                                    min={1}
                                    onChange={this.handleChangeNbrOfRepetition}
                                    />
                                ) : null}

                            </fieldset>
                            <div className="k-form-buttons">
                                <button
                                    type={"submit"}
                                    className="k-button"
                                    disabled={!formRenderProps.allowSubmit}
                                >
                                    Ajouter
                                </button>
                            </div>
                        </FormElement>
                    )}
                />
            </div>
        );
    }
}

export default CalendarAppointmentInterface;